What is this?
========================================================================

In the FACT telescope control software FACT++, separate processes are used to
monitor and control separate telescope sub systems. These processes communicate
with each other as well as with the human user via
the [DIM libary](http://dim.web.cern.ch/dim/) by implementing DimServers.
Each FACT++ DimServer provides certain DimCommands and publishes a number of DimServices.


 * [Installation](pyfactcontrol#markdown-header-installation)
 * [Controlling the FACT telescope](pyfactcontrol#markdown-header-controlling-the-fact-telescope)
 * [Why Finite State Machines?](pyfactcontrol#markdown-header-why-finite-state-machines)
 * [A Fact Dim Server](pyfactcontrol#markdown-header-a-fact-dim-server)

---


Installation
========================================================================

In order to use PyFactControl you need to have **dim** and **pydim** installed.
For convenience both are delivered with PyFactControl.

In order to verify your pydim installation just try:

    import pydim

in your (i)python console.

Building dim_v20r7
------------------
This dim version was downloaded from [download section](http://dim.web.cern.ch/dim/dim_unix.html)
of the[the DIM website](http://dim.web.cern.ch/dim/).


At the moment there is a problem wen trying to build dim on Ubuntu 14.04 64bit.
As a workaround one should add `MORECONNS=no` to the make call.
I usually don't "make install".

So the build process for dim in my case looks like this:

    :::shell-session
    cd $HOME/PyFactControl/dim_v20r7
    export OS=Linux
    source setup.sh
    make MORECONNS=no all

Building pydim
--------------
[pydim](http://dim.web.cern.ch/dim/PyDIM/doc/api/html/index.html)
is a python wrapper module for the C (not the C++ afaik) of the dim library.
It was also developed at CERN. It is currently being developed by Niko Neufeld as far as I know.

I downloaded a recent version for you and provide it here as well.

There is an INSTALL script, which is good to read, but since I usually don't really
install dim to `/usr/local/lib` the installation process needs to be adjusted a bit.

So the entire build process looked like this on my system:

    :::shell-session
    cd $HOME/PyFactControl/pydim_r4792
    export DIMDIR=$HOME/PyFactControl/dim_v20r7
    make

Instead of `make` you could also do `python setup.py build`.

Using Dim, Pydim and later also PyFactControl
----------------------------------------------

After building dim and pydim you might want to set these enviroment variables:
you can do it, e.g. in your .bashrc

    :::bash
    DIMDIR=$HOME/PyFactControl/dim_v20r7
    LD_LIBRARY_PATH=$DIMDIR/linux:$LD_LIBRARY_PATH

    PYDIMDIR=$HOME/PyFactControl/pydim_r4792/build/lib.linux-x86_64-2.7
    PYTHONPATH=$PYDIMDIR:$PYTHONPATH

    PATH=$DIMDIR/linux:$PATH


In order to test if everything was successfully built you should be able to
start the dim name server (DNS) on your local machine by typing:

    :::shell-session
    $ dns
    PID 6081 - Thu Jul 17 12:34:45 2014 -  DNS version 2007 starting up on <your_machine>

After starting the dns you will not return to the bash, instead the process
is supposed to block. The dns will run until you stop it by hitting Ctrl-C.
Keep it running in your current terminal like that, if you like to monitor its
output.

Now you can start the graphical user interface that comes with Dim. It's called **did**.

    :::shell-session
    $ did
    Couldn't open the following fonts:
        -*-HELVETICA-BOLD-R-*--*-100-*-*-*-*-ISO8859-1
    Couldn't open the following fonts:
        -*-COURIER-BOLD-R-*--*-100-*-*-*-*-ISO8859-1
    Couldn't open the following fonts:
        -*-HELVETICA-BOLD-R-*--*-120-*-*-*-*-ISO8859-1
    Couldn't open the following fonts:
        -*-TIMES-BOLD-R-*--*-100-*-*-*-*-ISO8859-1
    PID 6124 - Thu Jul 17 12:39:07 2014 - (FATAL) DIM_DNS_NODE undefined
    Exiting!
    $ export DIM_DNS_NODE=localhost
    $ did

As you see, one should set DIM_DNS_NODE to "localhost", so the dim programs know where
to look for the dns server, but I alsways forget that.

**TODO**: Explain how to use did to see there service list of DIM_DNS.

### pydim examples ###

In order to try out the pydim examples you should be able to cd into
`$HOME/PyFactControl/pydim_r4792/examples` and simply start your first
dim server by typing:

    :::shell-session
    $ python services-server.py

dont forget to `export DIM_DNS_NODE=localhost`.


In case you installed sphinx `sudp apt-get install python-sphinx`,
you can build the online docu yourself by:

    :::shell-session
    cd pydim_r4792/doc/guide
    make html
    firefox _build/html/index.html

---


Why Finite State Machines?
========================================================================

I try to motivate the software design decision to base the entire FACT++
process design on FSMs. I don't know if this motiviation is necessary.
If so, maybe Thomas should write it, rather than me.

In short the reasons are:

 * Safety: difficult to perform a dangerous action, when hardware is not in the right state.
 * Easier master control design.

---

Many different telescope subsystems exist, while some might be merely a
measurement device such as a network connected thermometer there is also
very delicate hardware that needs to be controlled in a secure manner.
Consider a powerful motor, which is able to destroy itself as well as the connected
telescope if not carefully stopped before shutting of its power.
One can imagine that not calling set_speed(0.) prior to calling stop()
might be a common mistake.
In addition it must be ensured that the controlling software does not,
in case of an exception, forget to hit the brakes.


A Fact Dim Server
========================================================================

When looking at the example dim server `pydim/examples/services-server.py`
using *did*, we saw that each dim server does not only provide the
services we explicitely gave it, but also some standard services are always provided.
These are:

 * services: SERVICE_LIST, CLIENT_LIST, VERSION_NUMBER
 * commands: SET_EXIT_HANDLER, EXIT

In FACT++ the list of services every server should provide has been enlarged.
(I was not able to find an explicit documentation of this, but its evident
when looking at ther servers using did)
The services every server should provide are:

 * SERVICE_DESC["C"] : similar to SERVICE_LIST it provides a kind of help text.
 * STATE["C"] : a textual representation of the state of the server.
 * STATE_LIST["C"] : a list of all possible states, and their numerical code.
 * MESSAGE["C"] : a general log message.

Since the SERVICE_DESC service provides a decent help text for each service,
even for itself, one can look up the meaning of the other services there.

## Fact++ like pydim-server ##

In the example file `factpp-like-server.py` you can find an example
implementation of the typical pydim example server in FACT++ style.
The file is already 200 lines long and one needs to make sure, that the
SERVICE_DESC string is kept up to date, when more services are added later.

Also, as you see, the server has not been implemented in a FSM design,
consequently the STATE service only contains rubbish data, we will
address that later.

In order to try it out just type:

    :::shell-session
    ~$ python PyFactControl/factpp-like-server.py

In order to stop the server, you'll have to fire up `did` and send the EXIT command
to your server.

---

In the file `factpp-base-class.py` you find another implementation.
The file contains the 3 classes:

 * Service : just a container for information regarding each service provided by a server.
 * FactppBase : base class, intended to make your life easier.
 * MyServer: specific example implementation.

As you see the number of lines shrank from over 200 to less than 100.
While MyServer does inherit features from FactppBase as well as from
threading.Thread, the latter is not mandatory but let's us interact
with the server from the Python CLI as well as via did.

For testing it, just execute thescript in an interactive python session

    :::shell-session
    ~$ python -i PyFactControl/factpp-base-class.py
    created MyServer with the name PETER
    type: 's.stop()' to stop the DimServer
    >>> s.stop()
    PETER is out of service... now.

Don't forget the `-i`.

## Dim Commands ##

Up to now we entirely ingored the existance of dim commands. dim commands are
a way of controlling a certain dim server. Every dim server provides the EXIT
command, which expects one integer argument, its simply the return value
of the server program.

