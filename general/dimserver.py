import pydim
import collections


class DimError(Exception):
    """ Base Dim Exception """
    pass

class DimGeneralServiceDesc(object):
    def __init__(self,
                name,
                description,
                callback,
                format = "",
                tag = 0,
                id = None
    ):
        self.name = name
        self.format = format
        self.description = description
        self.callback = callback
        self.tag = tag
        self.id = id


class DimServiceDesc(DimGeneralServiceDesc):
    def __init__(self, args):
        DimGeneralServiceDesc.__init__(self, args)
        self.type = 'service'
        self.constructor_func = pydim.dis_add_service


class DimCommandDesc(DimGeneralServiceDesc):
    def __init__(self, args):
        DimGeneralServiceDesc.__init__(self, args)
        self.type = 'command'
        self.constructor_func = pydim.dis_add_cmnd


class GeneralDimServer(object):
    def __init__(self, name, driver):
        if not pydim.dis_get_dns_node():
            raise DimError("No Dim DNS node found. Please set the environment variable DIM_DNS_NODE")

        self.name = name
        self.driver = driver

        self.services = None
        ### services should be a list of DimCommandDesc or DimServiceDesc
        self.__services_already_created = False

    def __create_services(self):
        if self.services is None:
            raise AttributeError("services List is None, but should be filled with DimService descriptions")

        for desc in self.services():
            id = desc.constructor_func( desc.name,
                            desc.format,
                            desc.callback,
                            desc.tag)
            desc.id = id
            if not id:
                raise DimError(
                    "An error occurred "
                    "while registering the general service '%s' of server '%s'" %
                    (desc.name, self.name)
                )

            # A service must be updated before using it.
            if desc.type == 'service':
                pydim.dis_update_service(desc.id)

        self.__services_already_created = True

    def start(self):
        if not self.__services_already_created:
            self.__create_services()
        pydim.dis_start_serving(self.name)

    def stop(self):
        pydim.dis_stop_serving(self.name)

