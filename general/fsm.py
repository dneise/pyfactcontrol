import threading
import Queue
import time

MACHINES = dict()

class FSMError(Exception):
    """Base FSM exception."""
    pass

class TransitionError(FSMError):
    """Transition exception."""
    pass

class StateError(FSMError):
    """State manipulation error."""
    pass

class FiniteStateMachine(threading.Thread):
    """Generic Threaded Finite State Machine."""

    DOT_ATTRS = {
        'directed': True,
        'strict': False,
        'rankdir': 'LR',
        'ratio': '0.3'
    }

    def __init__(self, name, default=True):
        """Construct a FSM."""
        threading.Thread.__init__(self)
        self.name = name
        self.inputs = list()
        self.states = list()
        self.init_state = None
        self.current_state = None

        MACHINES[name] = self
        if default:
            MACHINES['default'] = MACHINES[name]
        self.input_data = Queue.PriorityQueue(maxsize=10)

        self.stop = False

    @property
    def all_transitions(self):
        """Get transitions from states.

        Returns:
            List of three element tuples each consisting of
            (source state, input, destination state)
        """
        transitions = list()
        for src_state in self.states:
            for input_value, dst_state in src_state.items():
                transitions.append((src_state, input_value, dst_state))
        return transitions

    def transition(self, input_value):
        """Transition to the next state."""
        current = self.current_state
        if current is None:
            raise TransitionError('Current state not set.')

        if input_value in current:
            destination_state = current[input_value]
        else:
            print "command not allowed no transition"
            destination_state = current
        #destination_state = current.get(input_value, current.default_transition)

        if destination_state is None:
            raise TransitionError('Cannot transition from state %r'
                                  ' on input %r.' % (current.name, input_value))
        else:
            self.current_state = destination_state

    def reset(self):
        """Enter the Finite State Machine."""
        self.current_state = self.init_state


    def run(self):
        """Process input data."""
        self.reset()
        while True:
            try:
                item = self.input_data.get(timeout=0.3)
                self.transition(item)
                self.input_data.task_done()
            except Queue.Empty:
                pass
            if self.stop:
                break

    def stop(self):
        self.stop = True

class State(dict):
    """State class."""

    DOT_ATTRS = {
        'shape': 'circle',
        'height': '1.2',
    }
    DOT_ACCEPTING = 'doublecircle'

    def __init__(self, name, machine
                initial=False,
                #~ accepting=False,
                output=None,
                #on_entry=None,
                #on_exit=None,
                #on_input=None,
                #on_transition=None,
                #~ machine=None,
            ):
        """Construct a state."""
        dict.__init__(self)
        self.name = name
        #self.entry_action = on_entry
        #self.exit_action = on_exit
        #self.input_action = on_input
        #self.transition_action = on_transition
        self.output_values = [(None, output)]

        self.default_transition = None



        #~ if machine is None:
            #~ try:
                #~ machine = MACHINES['default']
            #~ except KeyError:
                #~ pass


        machine.states.append(self)
        #~ if accepting:
            #~ try:
                #~ machine.accepting_states.append(self)
            #~ except AttributeError:
                #~ raise StateError('The %r %s does not support accepting '
                                 #~ 'states.' % (machine.name,
                                 #~ machine.__class__.__name__))
        if initial:
            machine.init_state = self


    def entry_action(self):
        pass
    def exit_action(self):
        pass
    def input_action(self,arg):
        pass
    def transition_action(self,arg):
        pass


    #~ def entry_action(self):
        #~ print "entry_action of,", self.name
    #~ def exit_action(self):
        #~ print "exit_action of", self.name
    #~ def input_action(self,arg):
        #~ print "input_action( %r ) of %s" % (arg,self.name)
    #~ def transition_action(self,arg):
        #~ print "transition_action( %r ) of %s" % (arg,self.name)


    def __getitem__(self, input_value):
        """Make a transition to the next state."""
        next_state = dict.__getitem__(self, input_value)
        self.input_action(input_value)
        self.exit_action()
        self.transition_action(next_state)
        next_state.entry_action()
        return next_state

    def __setitem__(self, input_value, next_state):
        """Set a transition to a new state."""
        if not isinstance(next_state, State):
            raise StateError('A state must transition to another state,'
                             ' got %r instead.' % next_state)
        if isinstance(input_value, tuple):
            input_value, output_value = input_value
            self.output_values.append((input_value, output_value))
        dict.__setitem__(self, input_value, next_state)

    def __repr__(self):
        """Represent the object in a string."""
        return '<%r %s @ 0x%x>' % (self.name, self.__class__.__name__, id(self))

def draw_graph(fsm, title=None, filename=None):
    """Generate a DOT graph with pygraphviz."""
    try:
        import pygraphviz as pgv
    except ImportError:
        pgv = None
    if pgv is None:
        print "pygraphviz is not installed -- can't draw FSM Graph. Please install pygraphviz"
        return

    if title is None:
        title = fsm.name
    elif title is False:
        title = ''

    fsm_graph = pgv.AGraph(title=title, **fsm.DOT_ATTRS)
    fsm_graph.node_attr.update(State.DOT_ATTRS)

    for state in [fsm.init_state] + fsm.states:
        shape = State.DOT_ATTRS['shape']
        if hasattr(fsm, 'accepting_states'):
            if id(state) in [id(s) for s in fsm.accepting_states]:
                shape = state.DOT_ACCEPTING
        fsm_graph.add_node(n=state.name, shape=shape)

    fsm_graph.add_node('null', shape='plaintext', label=' ')
    fsm_graph.add_edge('null', fsm.init_state.name)

    for src, input_value, dst in fsm.all_transitions:
        label = str(input_value)
        fsm_graph.add_edge(src.name, dst.name, label=label)
    for state in fsm.states:
        if state.default_transition is not None:
            fsm_graph.add_edge(state.name, state.default_transition.name,
                               label='else')

    fsm_graph.draw(filename, prog='dot')

