import sys
import gps.driver
#import gps.dimserver
import gps.commandline


address = ("gps", 23)
driver = gps.driver.GpsArduinoDriver(address)
driver.start() # driver thread is started here

#dimserver = gps.dimserver.GpsDimServer(driver)
#dimserver.start()

shell = gps.commandline.GpsCmd(driver)
try:
    # HERE we block until the user leaves the shell.
    shell.cmdloop()
except:
    #In case a user hits Ctrl-C in panic, we catch that here and try
    #to gracefully stop the driver-thread and the dimserver-thread
    driver.stop()
    #dimserver.stop()
    #Here we re-raise the KeyboardInterrupt to the outside world.
    raise
