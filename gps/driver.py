import time
import socket

from general import fsm






class GpsArduinoDriver(object):
    def __init__(self, address=("gps", 23), socket_timeout=2.0 ):
        self._s = None #socket, will be setup by FSM
        self.address = address

        self.storage = dict()
        self.last_reply_to_set = ""
        self.veto_setting = 'veto_60'
        self.socket_timeout = socket_timeout

        self.fsm = fsm.FiniteStateMachine(self.__class__.__name__+'_fsm')

        # defining States of Driver FSM
        disconnected = fsm.State('DISCONNECTED', initial=True, machine=self.fsm)
        connecting = fsm.State('CONNECTING', machine=self.fsm)
        connected = fsm.State('CONNECTED', machine=self.fsm)
        getting_nema = fsm.State('getting_nema', machine=self.fsm)
        getting_status = fsm.State('getting_status', machine=self.fsm)
        setting_veto = fsm.State('setting_veto', machine=self.fsm)

        # Set up the transitions
        disconnected['connect_command'] = connecting
        connecting['connection_esablished'] = connected
        connecting['timeout'] = disconnected
        connected['get_status_command'] = getting_status
        connected['get_nema_command'] = getting_nema
        getting_status['recv_status'] = connected
        getting_status['timeout_status'] = connected
        getting_nema['recv_nema'] = connected
        getting_nema['timeout_nema'] = connected
        setting_veto['veto_sent'] = getting_status
        setting_veto['timeout_send_veto'] = getting_status
        connected['set_veto_command'] = setting_veto
        connected['disconnect_command'] = disconnected


        connecting.entry_action     = self.__connecting__entry_action
        getting_nema.entry_action   = self.__getting_nema__entry_action
        getting_status.entry_action = self.__getting_status__entry_action
        setting_veto.entry_action   = self.__setting_veto__entry_action
        disconnected.entry_action   = self.__disconnected__entry_action

    def __connecting__entry_action(self):
        self._s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._s.settimeout(self.socket_timeout)
        try:
            self._s.connect(self.address)
        except socket.timeout:
            self.fsm.input_data.put('timeout')
        else:
            self.fsm.input_data.put('connection_esablished')

    def __post(self, input, output_name, success_event, timeout_event):
        try:
            self._s.sendall(input)
            message = ""
            while True:
                if message.endswith("\r\n"):
                    break
                m = self._s.recv(4096)
                if len(m) == 0:
                    break
                message = message + m
            self.storage[output_name] = (time.time(), message[:-2])
        except socket.timeout:
            self.fsm.input_data.put(timeout_event)
        else:
            self.fsm.input_data.put(success_event)

    def __getting_nema__entry_action(self):
        self.__post(
                input="get_nema\r\n",
                output_name = "last_nema",
                success_event='recv_nema',
                timeout_event='timeout_nema',
                )

    def __getting_status__entry_action(self):
        self.__post(
                input="get_status\r\n",
                output_name = "last_status",
                success_event='recv_status',
                timeout_event='timeout_status',
                )

    def __setting_veto__entry_action(self):
        self.__post(
                input=self.veto_setting+"\r\n",
                output_name = "veto_sent_reply",
                success_event='veto_sent',
                timeout_event='timeout_send_veto',
                )

    def __disconnected__entry_action(self):
        self._s.sendall("quit\r\n")
        self._s.close()
        self._s = None

    def __str__(self):
        status = ""
        status += self.__class__.__name__ + "\n"
        status += "FSM status: " + repr(self.fsm.current_state) + '\n'
        for key in self.storage:
            status += key
            status += repr(self.storage[key])
            status += "\n"
        return status

    ### public interface
    def plot(self, filename=None):
        if filename is None:
            filename = self.__class__.__name__+"_fsm.png"
        fsm.draw_graph(self.fsm, filename[:-4], filename)

    def start(self):
        self.fsm.start()

    def stop(self, join=True):
        self.fsm.stop = True
        if join:
            # block until the FSM thread really stopped
            self.fsm.join()

    ### These methods can be used as pydim callback functions for DimCommands
    ### Each DimCommand callback function is called as: service_callback(params,tag)
    ### but these methods do not actually need parameters.
    ### ---> however, these ones do accept parameters
    def connect(self, args=None):
        self.fsm.input_data.put('connect_command')
    def disconnect(self, args=None):
        self.fsm.input_data.put('disconnect_command')

    def fetch_nema(self, args=None):
        self.fsm.input_data.put('get_nema_command')
    def fetch_status(self, args=None):
        self.fsm.input_data.put('get_status_command')

    def set_veto_60(self, args=None):
        self.veto_setting = 'veto_60'
        self.fsm.input_data.put('set_veto_command')
    def set_veto_on(self, args=None):
        self.veto_setting = 'veto_on'
        self.fsm.input_data.put('set_veto_command')
    def set_veto_off(self, args=None):
        self.veto_setting = 'veto_off'
        self.fsm.input_data.put('set_veto_command')

    ### These getters can be misused as PyDim callback functions for DimServices
    ### Each DimService callbacl function is called as: service_callback(tag)
    ### but normal getters do usually not accept parameters
    ### ---> these ones to accept parameters
    def get_nema(self, args=None):
        return self.storage['last_nema']
    def get_status(self, args=None):
        return self.storage['last_status']
    def get_state(self, args=None):
        return self.fsm.current_state.name
