from general.dimserver import GeneralDimServer, DimServiceDesc, DimCommandDesc

class GpsDimServer(GeneralDimServer):
    def __init__(self, driver, name="GPSCTRL"):
        GeneralDimServer.__init__(self, name, driver)

        self.services = [
            DimServiceDesc( name = 'nema',
                            format = "F;C",
                            description = ("|time[s]:unix epoch"
                                "|nema[text]: raw NEMA string from GPS module"),
                            callback = self.driver.get_nema ),
            DimServiceDesc( name = 'status',
                            format = "F;C",
                            description = ("|time[s]:unix epoch"
                                "|status[text]: answer of GpsArduino to 'get_status' request"),
                            callback = self.driver.get_status ),
            DimServiceDesc( name = 'state',
                            format = "C",
                            description = "|state[text]: current state of driver FSM.",
                            callback = self.driver.get_state ),
            DimCommandDesc( name = 'connect',
                            description = "setup ethernet connection to hardware. No parameters",
                            callback = self.driver.connect ),
            DimCommandDesc( name = 'disconnect',
                            description = "shutdown ethernet connection to hardware. No parameters",
                            callback = self.driver.disconnect ),
            DimCommandDesc( name = 'fetch_nema',
                            description = "fetch NEMA string from hardware. No parameters",
                            callback = self.driver.fetch_nema ),
            DimCommandDesc( name = 'fetch_status',
                            description = "fetch status of veto setting from hardware. No parameters",
                            callback = self.driver.fetch_status ),
            DimCommandDesc( name = 'set_veto_60',
                            description = "set trigger veto to 'once every full GMT minute'. No parameters",
                            callback = self.driver.set_veto_60 ),
            DimCommandDesc( name = 'set_veto_on',
                            description = "set trigger veto to always on, so no triggers will occur. No parameters",
                            callback = self.driver.set_veto_on ),
            DimCommandDesc( name = 'set_veto_off',
                            description = "set trigger veto off. So a trigger will occur every sec. No parameters",
                            callback = self.driver.set_veto_off ),
        ]
