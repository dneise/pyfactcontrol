"""
Each instanc of CLI is just an enhanced cmd.Cmd it is connected to a certain number of class instances, the slaves. For each slave the CLI inspects the available instance and classmethods and provides a commandline interface to the slaves methods.

A slave can be an instance if any class, but in the context of PyFactControl it will most probably be a hardware driver.

A typical PFC main program will instanciate a hardware driver. The driver will spawn a communication thread and then wait for user input. Users communicate with the driver by calling any of its (never blocking) methods.
When connecting a driver to a CLI instance and calling its blocking startloop() method, user interaction, i.e. method calling is defered to that CLI instance.

Keep in mind that only public driver methods are transfered to the CLI interface, thus private methods remain nicely hidden.

In case users prefer the real python interpreter or ipython over using the CLI there is nothing wrong with it. In that case just instanciate the drivers and use the python interpreter.
"""
import os # only for do_shell()
import cmd

import inspect

class CLI(cmd.Cmd, object):
    def __init__(self, driver):
        cmd.Cmd.__init__(self,completekey='tab')
        self.driver = driver
        # we get a list of all public names of methods of self.driver
        self.driver_methods = ["do_"+name for name, _ in inspect.getmembers(self.driver, predicate=inspect.ismethod) if name[0] != '_']

    def default(self, line):
        args = line.split()
        if hasattr( self.driver, args[0]):
            method = getattr(self.driver, args[0])
        try:
            method(*args[1:])
        except Exception as e:
            print "Error caught:", e

    def get_names(self):
        # list of strings of all members of this Cmd
        normal_names = super(CLI, self).get_names()
        return normal_names + self.driver_methods


    def do_print(self,arg):
        print self.driver

    def do_quit(self,arg=""):
        try:
            self.driver.stop()
        except:
            pass
        return True

    def do_shell(self, arg):
        return os.system(arg)
    def do_EOF(self,arg):
        print
        return self.do_quit()
    def emptyline(self):
        pass

class TestDriver(object):

    def __init__(self, name='Test'):
        self.name = name

    def func1(self, stuff=None):
        print "func1 called with: stuff=", stuff

    def func2(self, foo):
        print "func2 called with: foo=", foo


if __name__ == '__main__':
    test_driver =TestDriver(name="holger")
    shell = CLI(driver=test_driver)
    shell.cmdloop()
