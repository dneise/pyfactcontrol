#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import telnetlib
import time
import sys
import pydim
import threading
import socket
import select
import collections

from factpp import States, State, Service, FactppBase
class SkyBox( FactppBase, threading.Thread ):

    def __init__(self):
        threading.Thread.__init__(self)
        FactppBase.__init__(self, name="SKYBOX")
        self._stop = False

        self._angle_wrt_north = 0.
        self._diff = 0.
        self._brightness = 0.

        svc = Service(name="DATA",
                format='F:1;F:1;F:1', callback=self.data)
        svc.description="Data as read out form the skybox arduino. Read out occurs every 10sec only. So a human can try to read out as well."
        svc.parameter_description = collections.OrderedDict()
        svc.parameter_description['Angle'] = {'type':'float', 'text':'direction measured. Angle with respect to north. not yet calibrated! unit:deg'}
        svc.parameter_description['Diff'] = {'type':'float', 'text':'difference between two readings 5s apart [deg]'}
        svc.parameter_description['Brightness'] = {'type':'float', 'text':'light level of a sensor in the box, the low number the higher the light level. In the lab ~600 was dark.'}
        self.add_service( svc )

    def data(self, tag):
        return ((self._angle_wrt_north, self._diff, self._brightness) )

    def run(self):
        self.start_serving()
        while True:
            try:
		HOST = '10.0.100.202'
		PORT = 23
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                print time.asctime(), "socket: connecting"
		s.connect((HOST, PORT))
                print time.asctime(), "socket writing 'd\\r\\n'"
		s.sendall('d\r\n')
                print time.asctime(), "socket writing 'q\\r\\n'"
		s.sendall('q\r\n')
		message = ""
		print time.asctime(), " reading until arduino closes socket."
		starttime = time.time()
		while True:
			ready_to_read, _, __ = select.select([s, ], [], [], 10.0)
			if s in ready_to_read:
				data = s.recv(1024)
				if not data:
					break
				message += data
			else:
				# s neither has data, nor has been closed.
				# we have a problem. So let's send 'q\r\n' again.
				print time.asctime(), "REsending 'q\\r\\n'"
				s.sendall('q\r\n')

			
		s.close()
		message = message.split()
		print time.asctime(), "received:", message
                # message looks like this:
                # ['q', 'quit', 'd', 'direction', 'p=104', 'D=0', 'l=29']

                self._angle_wrt_north = float(message[4].split('=')[1])
                self._diff = float(message[5].split('=')[1])
                self._brightness = float(message[6].split('=')[1])
                print time.asctime(), "dim: updating"
                self.update()
                print time.asctime(), "me: sleeping 30sec"
                time.sleep(30)
            except Exception as e:
                print e

            if self._stop:
                print self.name, "is out of service... now."
                pydim.dis_stop_serving()
                break

    def stop(self):
        self._stop = True



if __name__ == "__main__":
    s = SkyBox()
    s.start()
    print "SkyBox is started"
    print "type: 's.stop()' to stop the DimServer"


