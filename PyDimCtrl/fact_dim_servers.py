from make_fix_dimservers import FactDimServer
class FTM_CONTROL ( FactDimServer): 
    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def disable_reports(self, *args):
        """ 
            Dim Command 
            disable sending rate reports
            status[bool]:disable or enable that the FTM sends rate reports (yes/no)
            B
        """ 
        self._cmd("DISABLE_REPORTS", *args)

    def toggle_pixel(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            S:1
        """ 
        self._cmd("TOGGLE_PIXEL", *args)

    def disable_all_patches_except(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            S:1
        """ 
        self._cmd("DISABLE_ALL_PATCHES_EXCEPT", *args)

    def set_trigger_multiplicity(self, *args):
        """ 
            Dim Command 
            Setup the Multiplicity condition for physcis triggers
            N[int]:Number of requirered coincident triggers from sum-patches (1-40)
            S:1
        """ 
        self._cmd("SET_TRIGGER_MULTIPLICITY", *args)

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def set_trigger_interval(self, *args):
        """ 
            Dim Command 
            Sets the trigger interval which is the distance between two consecutive artificial triggers.
            interval[ms]:The applied trigger interval in millisecond (min 1ms / 10bit)
            I:1
        """ 
        self._cmd("SET_TRIGGER_INTERVAL", *args)

    def enable_ftu(self, *args):
        """ 
            Dim Command 
            Enable or disable FTU
            Board[idx]:Index of the board (0-39), -1 for all
            Enable[bool]:Whether FTU should be enabled or disabled (yes/no)
            I:1;B:1
        """ 
        self._cmd("ENABLE_FTU", *args)

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def set_calibration_multiplicity(self, *args):
        """ 
            Dim Command 
            Setup the Multiplicity condition for artificial (calibration) triggers
            N[int]:Number of requirered coincident triggers from sum-patches (1-40)
            S:1
        """ 
        self._cmd("SET_CALIBRATION_MULTIPLICITY", *args)

    def reset_crate(self, *args):
        """ 
            Dim Command 
            Reset one of the crates 0-3
            crate[short]:Crate number to be reseted (0-3)
            S:1
        """ 
        self._cmd("RESET_CRATE", *args)

    def reset_configure(self, *args):
        """ 
            Dim Command 
            Reset states during a configuration or in case of configuration error
            
        """ 
        self._cmd("RESET_CONFIGURE", *args)

    def disable_pixel(self, *args):
        """ 
            Dim Command 
            (-1 or all)
            S:1
        """ 
        self._cmd("DISABLE_PIXEL", *args)

    def enable_veto(self, *args):
        """ 
            Dim Command 
            Enable veto line
            Enable[bool]:Enable veto (yes/no)
            B:1
        """ 
        self._cmd("ENABLE_VETO", *args)

    def enable_pixel(self, *args):
        """ 
            Dim Command 
            (-1 or all)
            S:1
        """ 
        self._cmd("ENABLE_PIXEL", *args)

    def set_calibration_window(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            S:1
        """ 
        self._cmd("SET_CALIBRATION_WINDOW", *args)

    def enable_ext1(self, *args):
        """ 
            Dim Command 
            Switch on the triggers through the first external line
            Enable[bool]:Enable ext1 trigger (yes/no)
            B:1
        """ 
        self._cmd("ENABLE_EXT1", *args)

    def set_trigger_window(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            S:1
        """ 
        self._cmd("SET_TRIGGER_WINDOW", *args)

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def set_threshold(self, *args):
        """ 
            Dim Command 
            Set the comparator threshold
            Patch[idx]:Index of the patch (0-159), -1 for all
            Threshold[counts]:Threshold to be set in binary counts
            I:2
        """ 
        self._cmd("SET_THRESHOLD", *args)

    def request_static_data(self, *args):
        """ 
            Dim Command 
            request transmission of static data from FTM to memory
            
        """ 
        self._cmd("REQUEST_STATIC_DATA", *args)

    def dynamic_data(self):
        """ 
            Dim Service 
            Regular reports sent by FTM
            FTMtimeStamp[us]:Time in microseconds, since trigger enabled or disabled
            OnTimeCounter[us]:Ontime, i.e. FTM processes triggers (e.g. No FAD busy)
            Temperatures[Nan]:not yet defined nor used (wanna be FTM onboard temps)
            TriggerPatchCounter[int]:counting since last update (prescaling)
            BoardsCounter[int]:FTU board counting after N out of 4 and since last update
            RateOverflow[bitpattern]:bits 0-4=patches overflow, 5=board overflow, 1 per board
            Prescaling[500ms]:Update rate of the rate counter
            CrcError[int]:Number of checksum error in RS485 communication
            State[int]:State value of the FTM firmware (cf. FTM doc)
            X:1;X:1;F:4;I:160;I:40;S:40;S:40;S:40;S:1
        """ 
        return self._get("DYNAMIC_DATA")

    def set_time_marker_delay(self, *args):
        """ 
            Dim Command 
            delay[int]:The applied time marker delay is: delay*4ns+8ns
            I:1
        """ 
        self._cmd("SET_TIME_MARKER_DELAY", *args)

    def configure(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            C
        """ 
        self._cmd("CONFIGURE", *args)

    def reconnect(self, *args):
        """ 
            Dim Command 
            (Re)connect ethernet connection to FTM, a new address can be given
            [host][string]:new ethernet address in the form <host:port>
            O
        """ 
        self._cmd("RECONNECT", *args)

    def enable_clock_conditioner(self, *args):
        """ 
            Dim Command 
            Enable clock conidtioner output in favor of time marker output
            Enable[bool]:Enable clock conditioner (yes/no)
            B:1
        """ 
        self._cmd("ENABLE_CLOCK_CONDITIONER", *args)

    def take_n_events(self, *args):
        """ 
            Dim Command 
            take n events (distribute n triggers)
            number[int]:Number of events to be taken
            I
        """ 
        self._cmd("TAKE_N_EVENTS", *args)

    def enable_group1_lpint(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            B:1
        """ 
        self._cmd("ENABLE_GROUP1_LPINT", *args)

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def trigger_rates(self):
        """ 
            Dim Service 
            Patch,Board,Camera trigger rates
            FTMtimeStamp[us]:Time in microseconds, since trigger enabled or disabled
            OnTimeCounter[us]:Effective on-time, ie. FTM triggers (eg. w/o busy)
            TriggerCounter[int]:Counter of triggers since enabled or disabled
            TriggerRate[Hz]:Trigger rate
            BoardRate[Hz]:Trigger rate of individual FTUs
            PatchRate[Hz]:Trigger rate of individual patches
            ElapsedTime[sec]:Time elapsed since previous report
            OnTime[sec]:OnTime elapsed since previous report
            X:1;X:1;I:1;F:1;F:40;F:160;F:1;F:1
        """ 
        return self._get("TRIGGER_RATES")

    def passport(self):
        """ 
            Dim Service 
            Info about the FTM and FPGA version
            BoardId[int]:BoardId, hexCode
            DNA[int]:DNA of the FTM board
            X:1;S:1
        """ 
        return self._get("PASSPORT")

    def set_n_out_of_4(self, *args):
        """ 
            Dim Command 
            Set the comparator threshold
            Board[idx]:Index of the board (0-39), -1 for all
            Threshold[counts]:Threshold to be set in binary counts
            I:2
        """ 
        self._cmd("SET_N_OUT_OF_4", *args)

    def set_intensity_lpext(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            S:1
        """ 
        self._cmd("SET_INTENSITY_LPEXT", *args)

    def enable_group2_lpext(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            B:1
        """ 
        self._cmd("ENABLE_GROUP2_LPEXT", *args)

    def enable_group1_lpext(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            B:1
        """ 
        self._cmd("ENABLE_GROUP1_LPEXT", *args)

    def set_clock_register(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            X:8
        """ 
        self._cmd("SET_CLOCK_REGISTER", *args)

    def load(self, *args):
        """ 
            Dim Command 
            Loads the static data (FTM configuration) from a file into memory and sends it to the FTM
            filename[string]:Filename (can include a path), .bin is automatically added
            C
        """ 
        self._cmd("LOAD", *args)

    def save(self, *args):
        """ 
            Dim Command 
            Saves the static data (FTM configuration) from memory to a file
            filename[string]:Filename (can include a path), .bin is automatically added
            C
        """ 
        self._cmd("SAVE", *args)

    def toggle_ftu(self, *args):
        """ 
            Dim Command 
            Toggle status of FTU (this is mainly meant to be used in the GUI)
            Board[idx]:Index of the board (0-39)
            I:1
        """ 
        self._cmd("TOGGLE_FTU", *args)

    def ftu_list(self):
        """ 
            Dim Service 
            Logs the changes of status of the FTUs
            FTMtimeStamp[us]:Time in microseconds
            ActiveFTU[bitpattern]:Description of enabled FTUs
            NumBoards[int]:Total number of enabled FTUs
            NumBoardsCrate[int]:Total number of enabled FTUs per crate
            DNA[hexCode]:Hex code identifier of FTUs
            Addr[bitpattern]:Crate address (hardware) of FTUs
            Ping[int]:Number of pings until FTU response
            X:1;X:1;S:1;C:4;X:40;C:40;C:40
        """ 
        return self._get("FTU_LIST")

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def set_register(self, *args):
        """ 
            Dim Command 
            set register to value
            addr[short]:Address of register
            val[short]:Value to be set
            I:2
        """ 
        self._cmd("SET_REGISTER", *args)

    def enable_ext2(self, *args):
        """ 
            Dim Command 
            Switch on the triggers through the second external line
            Enable[bool]:Enable ext2 trigger (yes/no)
            B:1
        """ 
        self._cmd("ENABLE_EXT2", *args)

    def enable_group2_lpint(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            B:1
        """ 
        self._cmd("ENABLE_GROUP2_LPINT", *args)

    def toggle_led(self, *args):
        """ 
            Dim Command 
            toggle led
            
        """ 
        self._cmd("TOGGLE_LED", *args)

    def counter(self):
        """ 
            Dim Service 
            Communication statistics to or from FTM control and FTM
            NumHeaders[int]:Num. of headers (any header) received by ftm control
            NumStaticData[int]:Num. of static data blocks (ftm and ftu settings)
            NumDynamicData[int]:Num. of dynamic data blocks (e.g. rates)
            NumFtuList[int]:Num. of FTU list (FTU identifiers, answer from ping)
            NumErrors[int]:Num. of error messages
            NumRegister[int]:Num. of answers from a single register accesess
            I:1;I:1;I:1;I:1;I:1;I:1
        """ 
        return self._get("COUNTER")

    def enable_patch(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            S:1
        """ 
        self._cmd("ENABLE_PATCH", *args)

    def set_dead_time(self, *args):
        """ 
            Dim Command 
            dead_time[int]:The applied dead time is: dead_time*4ns+8ns
            I:1
        """ 
        self._cmd("SET_DEAD_TIME", *args)

    def enable_trigger(self, *args):
        """ 
            Dim Command 
            Switch on the physics trigger
            Enable[bool]:Enable physics trigger (yes/no)
            B:1
        """ 
        self._cmd("ENABLE_TRIGGER", *args)

    def set_verbose(self, *args):
        """ 
            Dim Command 
            set verbosity state
            verbosity[bool]:disable or enable verbosity for received data (yes/no), except dynamic data
            B
        """ 
        self._cmd("SET_VERBOSE", *args)

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def set_trigger_delay(self, *args):
        """ 
            Dim Command 
            delay[int]:The applied trigger delay is: delay*4ns+8ns
            I:1
        """ 
        self._cmd("SET_TRIGGER_DELAY", *args)

    def reset_camera(self, *args):
        """ 
            Dim Command 
            Reset all crates. The commands are sent in the order 0,1,2,3
            
        """ 
        self._cmd("RESET_CAMERA", *args)

    def stop_trigger(self, *args):
        """ 
            Dim Command 
            stop a run (stop distributing triggers)
            
        """ 
        self._cmd("STOP_TRIGGER", *args)

    def set_hex_output(self, *args):
        """ 
            Dim Command 
            enable or disable hex output for received data
            hexout[bool]:disable or enable hex output for received data (yes/no)
            B
        """ 
        self._cmd("SET_HEX_OUTPUT", *args)

    def set_prescaling(self, *args):
        """ 
            Dim Command 
            Sets the FTU readout time intervals
            time[0.5s]:The interval is given in units of 0.5s, i.e. 1 means 0.5s, 2 means 1s, ...
            I:1
        """ 
        self._cmd("SET_PRESCALING", *args)

    def ping(self, *args):
        """ 
            Dim Command 
            send ping
            
        """ 
        self._cmd("PING", *args)

    def static_data(self):
        """ 
            Dim Service 
            Configuration of FTM and FTUs
            FTMtimeStamp[us]:Time in microseconds, since trigger enabled or disabled
            GeneralSettings[bitpattern]:Status of the FTM settings (cf. FTM doc)
            LEDStatus[bitpattern]:Not Used
            ActiveFTU[bitpattern]:List of enabled FTUs
            TriggerInterval[bitpattern]:Period of cal. and ped. events (cf. FTM doc)
            TriggerSeq[int]:Sequence of calib. and pedestal events (LPint, LPext, Ped)
            LPSettings[bitpattern]:Settings of LP, enabled int, ext, intensity int, ext
            PhysTrigMult[int]:N for N out of 40 logic on FTM (Physics)
            CalibTrigMult[int]: N for N out of 40 logic on FTM (Calib)
            PhysTrigWindow[ns]:Coincidence window for N out of 40 (Physics)
            CalibTrigWindow[ns]:Coincidence window for N out of 40 (Calib)
            TrigDelay[ns]:Trigger delay applied on FTM
            TMDelay[ns]:TM delay applied on FTM
            DeadTime[ns]:Dead time applied after each event on the FTM
            ClkCond[bitpattern]:Clock conditionner settings on the FTM (DRS sampling freq.)
            PixEnabled[bitpattern]:Enabled pixels, pckd in 90 shorts (160*9bits=180bytes)
            PatchThresh[DACcounts]:Threshold of the trigger patches
            Multiplicity[DACcounts]:N out of 4 logic settings per FTU
            Prescaling[500ms]:Update rate of the rate counter
            X:1;S:1;S:1;X:1;S:1;S:3;C:4;S:1;S:1;S:1;S:1;S:1;S:1;I:1;I:8;S:90;S:160;S:40;S:40
        """ 
        return self._get("STATIC_DATA")

    def error(self):
        """ 
            Dim Service 
            DESC in SERVICE_DESC is empty ?!
            X:1;S:1;S:28
        """ 
        return self._get("ERROR")

    def start_trigger(self, *args):
        """ 
            Dim Command 
            start a run (start distributing triggers)
            
        """ 
        self._cmd("START_TRIGGER", *args)

    def get_register(self, *args):
        """ 
            Dim Command 
            read register from address addr
            addr[short]:Address of register
            I
        """ 
        self._cmd("GET_REGISTER", *args)

    def disconnect(self, *args):
        """ 
            Dim Command 
            disconnect from ethernet
            
        """ 
        self._cmd("DISCONNECT", *args)

    def disable_all_pixels_except(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            S:1
        """ 
        self._cmd("DISABLE_ALL_PIXELS_EXCEPT", *args)

    def set_trigger_sequence(self, *args):
        """ 
            Dim Command 
            Setup the sequence of artificial triggers produced by the FTM
            Ped[short]:number of pedestal triggers in a row
            LPext[short]:number of triggers of the external light pulser
            LPint[short]:number of triggers of the internal light pulser
            S:3
        """ 
        self._cmd("SET_TRIGGER_SEQUENCE", *args)

    def set_dynamic_output(self, *args):
        """ 
            Dim Command 
            enable or disable output for received dynamic data (data is still broadcasted via Dim)
            dynout[bool]:disable or enable output for dynamic data (yes/no)
            B
        """ 
        self._cmd("SET_DYNAMIC_OUTPUT", *args)

    def disable_patch(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            S:1
        """ 
        self._cmd("DISABLE_PATCH", *args)

    def set_clock_frequency(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            S:1
        """ 
        self._cmd("SET_CLOCK_FREQUENCY", *args)

    def request_dynamic_data(self, *args):
        """ 
            Dim Command 
            request transmission of dynamic data block
            
        """ 
        self._cmd("REQUEST_DYNAMIC_DATA", *args)

    def set_intensity_lpint(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            S:1
        """ 
        self._cmd("SET_INTENSITY_LPINT", *args)

class RATE_SCAN ( FactDimServer): 
    def process_data(self):
        """ 
            Dim Service 
            Rate scan process data
            min[DAC]:Value at which scan was started
            max[DAC]:Value at which scan will end
            step[DAC]:Step size for scan
            I:1;I:1;I:1
        """ 
        return self._get("PROCESS_DATA")

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def start_threshold_scan(self, *args):
        """ 
            Dim Command 
            Start rate scan for the threshold in the defined range
            min[int]:Start value in DAC counts
            max[int]:Limiting value in DAC counts
            step[int]:Single step in DAC counts
            I:3
        """ 
        self._cmd("START_THRESHOLD_SCAN", *args)

    def print_cmd(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            
        """ 
        self._cmd("PRINT_CMD", *args)

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def set_reference_patch(self, *args):
        """ 
            Dim Command 
            Use the given patch trigger-rate as reference for the reolution
            patch[idx]:Index of the patch (360*crate+36*board+patch)
            I:1
        """ 
        self._cmd("SET_REFERENCE_PATCH", *args)

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def change_step_size(self, *args):
        """ 
            Dim Command 
            Change the step size during a ratescan in progress
            step[int]:Single step in DAC counts
            I:1
        """ 
        self._cmd("CHANGE_STEP_SIZE", *args)

    def data(self):
        """ 
            Dim Service 
            Id[s]:Start time used to identify measurement (UnixTime)
            Threshold[dac]:Threshold in DAC counts
            ElapsedTime[s]:Real elapsed time
            RelOnTime[ratio]:Relative on time
            TriggerRate[Hz]:Camera trigger rate
            BoardRate[Hz]:Board trigger rates
            PatchRate[Hz]:Patch trigger rates
            X:1;I:1;F:1;F:1;F:1;F:40;F:160
        """ 
        return self._get("DATA")

    def stop(self, *args):
        """ 
            Dim Command 
            Stop a ratescan in progress
            
        """ 
        self._cmd("STOP", *args)

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def start_n_out_of_4_scan(self, *args):
        """ 
            Dim Command 
            Start rate scan for N-out-of-4 in the defined range
            min[int]:Start value in DAC counts
            max[int]:Limiting value in DAC counts
            step[int]:Single step in DAC counts
            I:3
        """ 
        self._cmd("START_N_OUT_OF_4_SCAN", *args)

    def change_maximum(self, *args):
        """ 
            Dim Command 
            Change the maximum limit during a ratescan in progress
            max[int]:Limiting value in DAC counts
            I:1
        """ 
        self._cmd("CHANGE_MAXIMUM", *args)

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def set_reference_board(self, *args):
        """ 
            Dim Command 
            Use the given board trigger-rate as reference for the reolution
            board[idx]:Index of the board (4*crate+board)
            I:1
        """ 
        self._cmd("SET_REFERENCE_BOARD", *args)

    def set_reference_camera(self, *args):
        """ 
            Dim Command 
            Use the camera trigger rate as reference for the reolution
            
        """ 
        self._cmd("SET_REFERENCE_CAMERA", *args)

class RATE_CONTROL ( FactDimServer): 
    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def stop(self, *args):
        """ 
            Dim Command 
            Stop a calibration or ratescan in progress
            
        """ 
        self._cmd("STOP", *args)

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def print_cmd(self, *args):
        """ 
            Dim Command 
            Print current status
            
        """ 
        self._cmd("PRINT_CMD", *args)

    def calibrate(self, *args):
        """ 
            Dim Command 
            Start a search for a reasonable minimum global threshold
            
        """ 
        self._cmd("CALIBRATE", *args)

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def set_verbose(self, *args):
        """ 
            Dim Command 
            set verbosity state
            verbosity[bool]:disable or enable verbosity for received data (yes/no), except dynamic data
            B
        """ 
        self._cmd("SET_VERBOSE", *args)

    def set_target_rate(self, *args):
        """ 
            Dim Command 
            Set a target trigger rate for the calibration
            F:1
        """ 
        self._cmd("SET_TARGET_RATE", *args)

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def threshold(self):
        """ 
            Dim Service 
            Resulting threshold after calibration
            threshold[dac]:Resulting threshold from calibration
            S:1
        """ 
        return self._get("THRESHOLD")

    def set_min_threshold(self, *args):
        """ 
            Dim Command 
            Set a minimum threshold at which th rate control starts calibrating
            I:1
        """ 
        self._cmd("SET_MIN_THRESHOLD", *args)

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

class FEEDBACK ( FactDimServer): 
    def reference(self):
        """ 
            Dim Service 
            Amplitude reference value(s)Vref[mV]:Amplitude reference
            F:416
        """ 
        return self._get("REFERENCE")

    def enable_output(self, *args):
        """ 
            Dim Command 
            Enable sending of correction values caluclated by the control loop to the biasctrl
            B:1
        """ 
        self._cmd("ENABLE_OUTPUT", *args)

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def print_calibration(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            
        """ 
        self._cmd("PRINT_CALIBRATION", *args)

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def stop(self, *args):
        """ 
            Dim Command 
            Stop any control loop
            
        """ 
        self._cmd("STOP", *args)

    def start_temp_control(self, *args):
        """ 
            Dim Command 
            Start the temperature control loop
            offset[V]:Offset from the nominal temperature corrected value in Volts
            F:1
        """ 
        self._cmd("START_TEMP_CONTROL", *args)

    def set_kp(self, *args):
        """ 
            Dim Command 
            Set proportional constant Kp
            D:1
        """ 
        self._cmd("SET_KP", *args)

    def store_reference(self, *args):
        """ 
            Dim Command 
            Store the last (averaged) value as new reference (for debug purpose only)
            
        """ 
        self._cmd("STORE_REFERENCE", *args)

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def print_cmd(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            
        """ 
        self._cmd("PRINT_CMD", *args)

    def set_t(self, *args):
        """ 
            Dim Command 
            Set time-constant. (-1 to use the cycle time, i.e. the time for the last average cycle, instead)
            D:1
        """ 
        self._cmd("SET_T", *args)

    def start_feedback_control(self, *args):
        """ 
            Dim Command 
            Start the feedback control loop
            Num[short]:Number of events 'medianed' to calculate the correction value
            S:1
        """ 
        self._cmd("START_FEEDBACK_CONTROL", *args)

    def set_current_request_interval(self, *args):
        """ 
            Dim Command 
            interval[ms]:Interval between two current requests in modes which need that.
            
        """ 
        self._cmd("SET_CURRENT_REQUEST_INTERVAL", *args)

    def set_reference(self, *args):
        """ 
            Dim Command 
            Set a new global reference value (for debug purpose only)
            F:1
        """ 
        self._cmd("SET_REFERENCE", *args)

    def start_global_feedback(self, *args):
        """ 
            Dim Command 
            Start the global feedback control loopNum[short]:Number of events averaged to calculate the correction value
            S:1
        """ 
        self._cmd("START_GLOBAL_FEEDBACK", *args)

    def deviation(self):
        """ 
            Dim Service 
            Control loop information
            DeltaAmpl[mV]:Amplitude offset measures
            DeltaBias[mV]:Correction value calculated
            DeltaTemp[mV]:Correction calculated from temperature
            DeltaUser[mV]:Additional offset specified by user
            F:416;F:416;F:1;F:1
        """ 
        return self._get("DEVIATION")

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def calibration(self):
        """ 
            Dim Service 
            Current offsets
            Avg[uA]:Average offset
            Rms[uA]:Rms of offset
            R[Ohm]:Measured calibration resistor
            F:416;F:416;F:416
        """ 
        return self._get("CALIBRATION")

    def set_kd(self, *args):
        """ 
            Dim Command 
            Set derivative constant Kd
            D:1
        """ 
        self._cmd("SET_KD", *args)

    def set_ki(self, *args):
        """ 
            Dim Command 
            Set integral constant Ki
            D:1
        """ 
        self._cmd("SET_KI", *args)

    def start_current_control(self, *args):
        """ 
            Dim Command 
            Start the current/temperature control loop
            offset[V]:Offset from the nominal current/temperature corrected value in Volts
            F:1
        """ 
        self._cmd("START_CURRENT_CONTROL", *args)

    def calibrate_currents(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            
        """ 
        self._cmd("CALIBRATE_CURRENTS", *args)

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

class LID_CONTROL ( FactDimServer): 
    def data(self):
        """ 
            Dim Service 
            status[bool]:Lid1/2 open or closed
            I[A]:Lid1/2 current
            P[dac]:Lid1/2 hall sensor position in averaged dac counts
            S:2;F:2;F:2
        """ 
        return self._get("DATA")

    def close(self, *args):
        """ 
            Dim Command 
            Close the lids
            
        """ 
        self._cmd("CLOSE", *args)

    def post(self, *args):
        """ 
            Dim Command 
            set verbosity state
            verbosity[bool]:disable or enable verbosity for received data (yes/no), except dynamic data
            C
        """ 
        self._cmd("POST", *args)

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def open(self, *args):
        """ 
            Dim Command 
            Open the lids
            
        """ 
        self._cmd("OPEN", *args)

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def set_verbose(self, *args):
        """ 
            Dim Command 
            set verbosity state
            verbosity[bool]:disable or enable verbosity for received data (yes/no), except dynamic data
            B
        """ 
        self._cmd("SET_VERBOSE", *args)

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

class FSC_CONTROL ( FactDimServer): 
    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def voltage(self):
        """ 
            Dim Service 
            t[s]:FSC uptime
            FAD_Ud[V]:FAD digital (crate 0-3)
            FAD_Up[V]:FAD positive (crate 0-3)
            FAD_Un[V]:FAD negative (crate 0-3)
            FPA_Ud[V]:FPA digital (crate 0-3)
            FPA_Up[V]:FPA positive (crate 0-3)
            FPA_Un[V]:FPA negative (crate 0-3)
            ETH_U[V]:Ethernet switch (pos/neg)
            FTM_U[V]:FTM - trigger master (pos/neg)
            FFC_U[V]:FFC
            FLP_U[V]:FLP - light pulser
            F:1;F:4;F:4;F:4;F:4;F:4;F:4;F:2;F:2;F:1;F:1
        """ 
        return self._get("VOLTAGE")

    def current(self):
        """ 
            Dim Service 
            t[s]:FSC uptime
            FAD_Id[A]:FAD digital (crate 0-3)
            FAD_Ip[A]:FAD positive (crate 0-3)
            FAD_In[A]:FAD negative (crate 0-3)
            FPA_Id[A]:FPA digital (crate 0-3)
            FPA_Ip[A]:FPA positive (crate 0-3)
            FPA_In[A]:FPA negative (crate 0-3)
            ETH_I[A]:Ethernet switch (pos/neg)
            FTM_I[A]:FTM - trigger master (pos/neg)
            FFC_I[A]:FFC
            FLP_I[A]:FLP - light pulser
            F:1;F:4;F:4;F:4;F:4;F:4;F:4;F:2;F:2;F:1;F:1
        """ 
        return self._get("CURRENT")

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def disconnect(self, *args):
        """ 
            Dim Command 
            disconnect from ethernet
            
        """ 
        self._cmd("DISCONNECT", *args)

    def dump_stream(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            B:1
        """ 
        self._cmd("DUMP_STREAM", *args)

    def humidity(self):
        """ 
            Dim Service 
            t[s]:FSC uptime
            H[%]:Humidity sensors readout
            F:1;F:4
        """ 
        return self._get("HUMIDITY")

    def temperature(self):
        """ 
            Dim Service 
            t[s]:FSC uptime
            T_sens[deg C]:Sensor compartment temperatures
            T_crate[deg C]:Temperatures crate 0 (back/front), 1 (b/f), 2 (b/f), 3 (b/f)
            T_ps[deg C]:Temp power supplies crate 0 (back/front), 1, 2, 3
            T_aux[deg C]:Auxiliary power supply temperatures FTM (top/bottom), FSC (t/b)
            T_back[deg C]:FTM backpanel temperatures FTM (top/bottom), FSC (top/bottom)
            T_eth[deg C]:Ethernet switches temperatures top (front/back), bottom (f/b)
            F:1;F:31;F:8;F:8;F:4;F:4;F:4
        """ 
        return self._get("TEMPERATURE")

    def set_verbose(self, *args):
        """ 
            Dim Command 
            set verbosity state
            verbosity[bool]:disable or enable verbosity for received data (yes/no), except dynamic data
            B:1
        """ 
        self._cmd("SET_VERBOSE", *args)

    def reconnect(self, *args):
        """ 
            Dim Command 
            (Re)connect ethernet connection to FTM, a new address can be given
            [host][string]:new ethernet address in the form <host:port>
            O
        """ 
        self._cmd("RECONNECT", *args)

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

class PWR_CONTROL ( FactDimServer): 
    def set_verbose(self, *args):
        """ 
            Dim Command 
            Set verbosity state
            verbosity[bool]:disable or enable verbosity for interpreted data (yes/no)
            B:1
        """ 
        self._cmd("SET_VERBOSE", *args)

    def camera_power(self, *args):
        """ 
            Dim Command 
            Switch camera power
            power[bool]:Switch camera power 'on' or 'off'
            B:1
        """ 
        self._cmd("CAMERA_POWER", *args)

    def set_debug_rx(self, *args):
        """ 
            Dim Command 
            Set debux-rx state
            debug[bool]:dump received text and parsed text to console (yes/no)
            B:1
        """ 
        self._cmd("SET_DEBUG_RX", *args)

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def toggle_drive(self, *args):
        """ 
            Dim Command 
            Toggle drive power
            
        """ 
        self._cmd("TOGGLE_DRIVE", *args)

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def data(self):
        """ 
            Dim Service 
            water_lvl[bool]:Water level ok
            water_flow[bool]:Water flowing
            pwr_24V[bool]:24V power enabled
            pwr_pump[bool]:Pump power enabled
            pwr_bias[bool]:Bias power enabled
            pwr_drive[bool]:Drive power enabled (command value)
            main_drive[bool]:Drive manual main switch on
            feedback_drive[bool]:Drive power on (feedback value)
            C:1;C:1;C:1;C:1;C:1;C:1;C:1;C:1
        """ 
        return self._get("DATA")

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def post(self, *args):
        """ 
            Dim Command 
            set verbosity state
            verbosity[bool]:disable or enable verbosity for received data (yes/no), except dynamic data
            C
        """ 
        self._cmd("POST", *args)

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

class DATA_LOGGER ( FactDimServer): 
    def filename_run(self):
        """ 
            Dim Service 
            Path and base name used for the run files.
            Type[int]:type of open files (1=log, 2=rep, 4=fits)
            Name[string]:path and base file name
            I:1;C
        """ 
        return self._get("FILENAME_RUN")

    def stop(self, *args):
        """ 
            Dim Command 
            Stop all data logging, close all files.
            
        """ 
        self._cmd("STOP", *args)

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def enable_numsubs_service(self, *args):
        """ 
            Dim Command 
            Switch the service which distributes information about the number of subscriptions and open files on or off.
            Enable[bool]:Enable of disable NUM_SUBS service (yes/no).
            B:1
        """ 
        self._cmd("ENABLE_NUMSUBS_SERVICE", *args)

    def enable_filename_services(self, *args):
        """ 
            Dim Command 
            Switch service which distributes information about the open files on or off.
            Enable[bool]:Enable of disable filename services (yes/no).
            B:1
        """ 
        self._cmd("ENABLE_FILENAME_SERVICES", *args)

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def set_run_timeout(self, *args):
        """ 
            Dim Command 
            Set the timeout delay for old run numbers.
            timeout[min]:Time out in minutes after which files for expired runs are closed.
            L:1
        """ 
        self._cmd("SET_RUN_TIMEOUT", *args)

    def num_subs(self):
        """ 
            Dim Service 
            Num. open files + num. subscribed services
            NSubAndOpenFiles[int]:Num. of subs and open files
            I:2
        """ 
        return self._get("NUM_SUBS")

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def set_statistics_update_interval(self, *args):
        """ 
            Dim Command 
            Interval in which the data-logger statistics service (STATS) is updated.
            Interval[ms]:Value in milliseconds (<=0: no update).
            S:1
        """ 
        self._cmd("SET_STATISTICS_UPDATE_INTERVAL", *args)

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def set_debug_mode(self, *args):
        """ 
            Dim Command 
            Switch debug mode on or off. Debug mode prints information about every service written to a file.
            Enable[bool]:Enable of disable debug mode (yes/no).
            B:1
        """ 
        self._cmd("SET_DEBUG_MODE", *args)

    def print_info(self, *args):
        """ 
            Dim Command 
            Print information about the internal status of the data logger.
            
        """ 
        self._cmd("PRINT_INFO", *args)

    def start(self, *args):
        """ 
            Dim Command 
            Start the nightly logging. Nightly file location must be specified already
            
        """ 
        self._cmd("START", *args)

    def stop_run_logging(self, *args):
        """ 
            Dim Command 
            Go from the wait for run to nightly open state.
            
        """ 
        self._cmd("STOP_RUN_LOGGING", *args)

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def stats(self):
        """ 
            Dim Service 
            Statistics about size written
            X:4
        """ 
        return self._get("STATS")

    def reset(self, *args):
        """ 
            Dim Command 
            Transition to exit error states. Closes the any open file.
            
        """ 
        self._cmd("RESET", *args)

    def start_run_logging(self, *args):
        """ 
            Dim Command 
            Go to waiting for run number state. In this state with any received run-number a new file is opened.
            
        """ 
        self._cmd("START_RUN_LOGGING", *args)

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def filename_nightly(self):
        """ 
            Dim Service 
            Path and base name used for the nightly files.
            Type[int]:type of open files (1=log, 2=rep, 4=fits)
            Name[string]:path and base file name
            I:1;C
        """ 
        return self._get("FILENAME_NIGHTLY")

class TNG_WEATHER ( FactDimServer): 
    def set_verbose(self, *args):
        """ 
            Dim Command 
            set verbosity state
            verbosity[bool]:disable or enable verbosity for received data (yes/no), except dynamic data
            B
        """ 
        self._cmd("SET_VERBOSE", *args)

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def dust(self):
        """ 
            Dim Service 
            Dust[ug/m^3]:Dust (total)
            F:1
        """ 
        return self._get("DUST")

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def data(self):
        """ 
            Dim Service 
            T_10M[deg C]:Temperature 10m above ground
            T_5M[deg C]:Temperature 5m above ground
            T_2M[deg C]:Temperature 2m above ground
            T_0[deg C]:Temperature at ground
            T_dew[deg C]:Dew point
            H[%]:Humidity
            P[mbar]:Air pressure
            v[km/h]:Wind speed
            d[deg]:Wind direction (N-E)
            DeltaM1
            Dust[ug/m^3]:Dust (total)
            Seeing[W/m^2]:Seeing
            F:1;F:1;F:1;F:1;F:1;F:1;F:1;F:1;F:1;F:1;F:1;F:1
        """ 
        return self._get("DATA")

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

class MAGIC_LIDAR ( FactDimServer): 
    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def data(self):
        """ 
            Dim Service 
            Zd[deg]:Pointing direction zenith distance
            Az[deg]:Pointing direction azimuth
            CHE[km]:Cloud height
            COT[1]:Cloud optical transparency at 532nm
            PBL[100]:Planetary boundary layer normalized to 100 for clear nights
            F:1;F:1;F:1;F:1;F:1
        """ 
        return self._get("DATA")

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def set_verbose(self, *args):
        """ 
            Dim Command 
            set verbosity state
            verbosity[bool]:disable or enable verbosity for received data (yes/no), except dynamic data
            B
        """ 
        self._cmd("SET_VERBOSE", *args)

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

class AGILENT_CONTROL ( FactDimServer): 
    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def set_verbose(self, *args):
        """ 
            Dim Command 
            set verbosity state
            verbosity[bool]:disable or enable verbosity for received data (yes/no)
            B:1
        """ 
        self._cmd("SET_VERBOSE", *args)

    def set_power(self, *args):
        """ 
            Dim Command 
            Enable or disable power output
            output[bool]:set power output to 'on' or 'off'
            B:1
        """ 
        self._cmd("SET_POWER", *args)

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def power_cycle(self, *args):
        """ 
            Dim Command 
            Power cycle the power output
            delay[short]:Defines the delay between switching off and on.
            S:1
        """ 
        self._cmd("POWER_CYCLE", *args)

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def reconnect(self, *args):
        """ 
            Dim Command 
            (Re)connect ethernet connection to Agilent, a new address can be given
            [host][string]:new ethernet address in the form <host:port>
            O
        """ 
        self._cmd("RECONNECT", *args)

    def set_debug_rx(self, *args):
        """ 
            Dim Command 
            set debug state
            debug[bool]:disable or enable verbosity for received raw data (yes/no)
            B:1
        """ 
        self._cmd("SET_DEBUG_RX", *args)

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def disconnect(self, *args):
        """ 
            Dim Command 
            disconnect from ethernet
            
        """ 
        self._cmd("DISCONNECT", *args)

    def data(self):
        """ 
            Dim Service 
            U_nom[V]: Nominal output voltage
            U_mes[V]: Measured output voltage
            I_mes[A]: Measured current
            I_max[A]: Current limit
            F:1;F:1;F:1;F:1
        """ 
        return self._get("DATA")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

class FAD_CONTROL ( FactDimServer): 
    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def enable_continous_trigger(self, *args):
        """ 
            Dim Command 
            Enable continous (internal) trigger.
            B:1
        """ 
        self._cmd("ENABLE_CONTINOUS_TRIGGER", *args)

    def event_data(self):
        """ 
            Dim Service 
            DESC in SERVICE_DESC is empty ?!
            F:1440;F:1440;F:1440;F:1440
        """ 
        return self._get("EVENT_DATA")

    def print_event(self, *args):
        """ 
            Dim Command 
            Print (last) event
            board[short]:slot from which the event should be printed (-1 for all)
            S:1
        """ 
        self._cmd("PRINT_EVENT", *args)

    def block_transmission(self, *args):
        """ 
            Dim Command 
            Blocks the transmission of commands to the given slot. Use with care! For debugging pupose only!
            slot[short]:Slot to which the command transmission should be blocked (0-39)
            enable[bool]:Whether the command transmission should be blockes (yes) or allowed (no)
            S:1;B:1
        """ 
        self._cmd("BLOCK_TRANSMISSION", *args)

    def set_debug_tx(self, *args):
        """ 
            Dim Command 
            Enable or disable the output of messages in case of successfull data transmission to the boards.
            debug[bool]:disable or enable debug output for transmitted data (yes/no)
            B:1
        """ 
        self._cmd("SET_DEBUG_TX", *args)

    def dac(self):
        """ 
            Dim Service 
            DAC settings of each FAD board
            DAC[int]:DAC counts, sequentially DAC 0 board 0, 0/1, 0/2... (plus min max)
            S:336
        """ 
        return self._get("DAC")

    def status(self):
        """ 
            Dim Service 
            Status of FAD boards
            status[bitpattern]:Status of each FAD board. Maybe buggy
            S:42
        """ 
        return self._get("STATUS")

    def disconnect(self, *args):
        """ 
            Dim Command 
            Disconnect a connected slot.
            S:1
        """ 
        self._cmd("DISCONNECT", *args)

    def set_max_memory(self, *args):
        """ 
            Dim Command 
            Set maximum memory buffer size allowed to be consumed by the EventBuilder to buffer events.
            memory[short]:Buffer size in Mega-bytes.
            S:1
        """ 
        self._cmd("SET_MAX_MEMORY", *args)

    def firmware_version(self):
        """ 
            Dim Service 
            Firmware version number of fad boards
            firmware[float]:Version number of firmware, for each board. 40=min, 41=max
            F:42
        """ 
        return self._get("FIRMWARE_VERSION")

    def load_drs_calibration(self, *args):
        """ 
            Dim Command 
            Load a DRS calibration file
            absolute path
            C
        """ 
        self._cmd("LOAD_DRS_CALIBRATION", *args)

    def add_address(self, *args):
        """ 
            Dim Command 
            Add the address of a DRS4 board to the first free slot
            IP[string]:address in the format <address:port>
            C
        """ 
        self._cmd("ADD_ADDRESS", *args)

    def drs_runs(self):
        """ 
            Dim Service 
            DESC in SERVICE_DESC is empty ?!
            I:1;I:3
        """ 
        return self._get("DRS_RUNS")

    def enable_trigger_line(self, *args):
        """ 
            Dim Command 
            Incoming triggers can be accepted/will not be accepted
            B:1
        """ 
        self._cmd("ENABLE_TRIGGER_LINE", *args)

    def enable_dwrite(self, *args):
        """ 
            Dim Command 
            Set Dwrite (possibly high / always low)
            B:1
        """ 
        self._cmd("ENABLE_DWRITE", *args)

    def feedback_data(self):
        """ 
            Dim Service 
            DESC in SERVICE_DESC is empty ?!
            F:1440
        """ 
        return self._get("FEEDBACK_DATA")

    def set_file_format(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            S:1
        """ 
        self._cmd("SET_FILE_FORMAT", *args)

    def reference_clock(self):
        """ 
            Dim Service 
            Reference clock of FAD boards
            refClocks[t]:ref clocks of FAD boards. 40=min, 41=max
            I:42
        """ 
        return self._get("REFERENCE_CLOCK")

    def phase_shift(self, *args):
        """ 
            Dim Command 
            Adjust ADC phase (in 'steps')
            phase[short]
            S:1
        """ 
        self._cmd("PHASE_SHIFT", *args)

    def enable_busy_off(self, *args):
        """ 
            Dim Command 
            Set BUSY continously low
            B:1
        """ 
        self._cmd("ENABLE_BUSY_OFF", *args)

    def set_data_output(self, *args):
        """ 
            Dim Command 
            Enable or disable printing of the received adc data to the console
            dataout[bool]:disable or enable data output for received data (yes/no)
            B:1
        """ 
        self._cmd("SET_DATA_OUTPUT", *args)

    def block_transmission_range(self, *args):
        """ 
            Dim Command 
            Blocks the transmission of commands to the given range of slots. Use with care! For debugging pupose only!
            first[short]:First slot to which the command transmission should be blocked (0-39)
            last[short]:Last slot to which the command transmission should be blocked (0-39)
            enable[bool]:Whether the command transmission should be blockes (yes) or allowed (no)
            S:2;B:1
        """ 
        self._cmd("BLOCK_TRANSMISSION_RANGE", *args)

    def events(self):
        """ 
            Dim Service 
            Event counts
            evtsCount[int]:Num evts cur. run, total (all run), evt ID, trig. Num
            I:4
        """ 
        return self._get("EVENTS")

    def ignore_events_range(self, *args):
        """ 
            Dim Command 
            Instructs the event-builder to ignore events from the given slot but still read the data from the socket.
            first[short]:First slot from which the data should be ignored when building events
            last[short]:Last slot from which the data should be ignored when building events
            enable[bool]:Whether the event builder should ignore data from this slot (yes) or allowed (no)
            S:2;B:1
        """ 
        self._cmd("IGNORE_EVENTS_RANGE", *args)

    def send_data(self, *args):
        """ 
            Dim Command 
            Send a command with data to the FADs. Values between 0 and 0xffff are allowed.
            command[uint16]:Command to be transmittted.
            data[uint16]:Data to be sent with the command.
            I:2
        """ 
        self._cmd("SEND_DATA", *args)

    def abort(self, *args):
        """ 
            Dim Command 
            Immediately abort EventBuilder thread and disconnect all slots.
            
        """ 
        self._cmd("ABORT", *args)

    def reset_event_counter(self, *args):
        """ 
            Dim Command 
            Reset the FAD boards' event counter to 0.
            
        """ 
        self._cmd("RESET_EVENT_COUNTER", *args)

    def set_hex_output(self, *args):
        """ 
            Dim Command 
            Enable or disable hex output for received data
            hexout[bool]:disable or enable hex output for received data (yes/no)
            B:1
        """ 
        self._cmd("SET_HEX_OUTPUT", *args)

    def statistics1(self):
        """ 
            Dim Service 
            Event Builder status for GUI display
            threadInfo[int]:Number of read, proc and writes
            bufferInfo[int]:Events in buffer, incomp., comp., tot., max past cycle, total
            memInfo[int]:total buf. mem, used mem, max used, max past cycle
            EvtCnt[int]:Number of events skipped, written, with errors
            badRoi[int]:Num boards with wrong ROI in event, run or board
            badRoiBoard[int]:Num boards with wrong ROI
            deltaT[ms]:Time in ms for rates
            rateNew[int]:Number of new start events received
            numConn[int]:Number of connections per board
            errConn[int]:IO errors per board
            rateBytes[int]:Bytes read this cycle
            totBytes[int]:Bytes read (counter)
            I:3;I:5;X:4;I:3;I:3;I:40;I:1;I:2;C:40;I:40;I:40;X:40
        """ 
        return self._get("STATISTICS1")

    def enable_sclk(self, *args):
        """ 
            Dim Command 
            Set SCLK
            B:1
        """ 
        self._cmd("ENABLE_SCLK", *args)

    def enable_busy_on(self, *args):
        """ 
            Dim Command 
            Set BUSY constantly high (has priority over BUSY_OFF)
            B:1
        """ 
        self._cmd("ENABLE_BUSY_ON", *args)

    def set_region_of_interest(self, *args):
        """ 
            Dim Command 
            Set region-of-interest to value
            channel[short]:Channel on each chip for which the ROI is set (0-8), -1 for all
            val[short]:Value to be set
            I:2
        """ 
        self._cmd("SET_REGION_OF_INTEREST", *args)

    def set_trigger_rate(self, *args):
        """ 
            Dim Command 
            Enable continous trigger
            I:1
        """ 
        self._cmd("SET_TRIGGER_RATE", *args)

    def statistics2(self):
        """ 
            Dim Service 
            Event Builder status, events oriented
            reset[int]:If increased, reset all counters
            numRead[int]:How often sucessful read from N sockets per loop
            gotByte[int]:number of bytes read per board
            gotErr[int]:number of com. errors per board
            evtStat[int]:number of evts read, completed, with errors, incomplete
            procStat[int]:num. of evts proc., w probs, acc. or rej. by SW trigger
            feedStat[int]:number of evts used or rejected by feedback system
            wrtStat[int]:number of evts written to disk, with errors
            runStat[int]:number of run opened, closed, with open or close errors
            numConn[int]:number of sockets successfully opened per board
            I:1;I:280;X:40;I:40;I:4;I:4;I:2;I:2;I:3;C:40
        """ 
        return self._get("STATISTICS2")

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def send_cmd(self, *args):
        """ 
            Dim Command 
            Send a command to the FADs. Values between 0 and 0xffff are allowed.
            command[uint16]:Command to be transmittted.
            I:1
        """ 
        self._cmd("SEND_CMD", *args)

    def stop(self, *args):
        """ 
            Dim Command 
            Stop EventBuilder thread (still write buffered events) and disconnect all slots.
            
        """ 
        self._cmd("STOP", *args)

    def connect(self, *args):
        """ 
            Dim Command 
            Connect a disconnected slot.
            S:1
        """ 
        self._cmd("CONNECT", *args)

    def send_single_trigger(self, *args):
        """ 
            Dim Command 
            Issue software triggers
            
        """ 
        self._cmd("SEND_SINGLE_TRIGGER", *args)

    def dump_recv(self, *args):
        """ 
            Dim Command 
            For debugging purpose: the times when data has been receives are dumped to a file.
            switch[bool]:Enable (yes) or disable (no)
            B:1
        """ 
        self._cmd("DUMP_RECV", *args)

    def enable_drs(self, *args):
        """ 
            Dim Command 
            Switch Domino wave
            B:1
        """ 
        self._cmd("ENABLE_DRS", *args)

    def start_drs_calibration(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            
        """ 
        self._cmd("START_DRS_CALIBRATION", *args)

    def start(self, *args):
        """ 
            Dim Command 
            Start EventBuilder thread and connect all valid slots.
            
        """ 
        self._cmd("START", *args)

    def set_dac_value(self, *args):
        """ 
            Dim Command 
            Set DAC numbers in range to value
            addr[short]:Address of register (-1 for all)
            val[short]:Value to be set
            I:2
        """ 
        self._cmd("SET_DAC_VALUE", *args)

    def drs_calibration(self):
        """ 
            Dim Service 
            DESC in SERVICE_DESC is empty ?!
            I:1;I:3;F:1474560;F:1474560;F:1474560;F:1474560;F:1474560;F:1474560;F:163840;F:163840
        """ 
        return self._get("DRS_CALIBRATION")

    def dna(self):
        """ 
            Dim Service 
            DNA of FAD boards
            DNA[hex]:Hex identifier of each FAD board
            X:40
        """ 
        return self._get("DNA")

    def configure(self, *args):
        """ 
            Dim Command 
            Configure a new run. If the internla trigger is enabled this might even start a new run.
            time_max[s]:Maximum time before the run is closed in seconds (0: unlimited)
            num_max[int]:Maximum number of events before the run is closed in seconds (0: unlimited)
            run_type[string]:Run type which describes the runs
            X:2;C
        """ 
        self._cmd("CONFIGURE", *args)

    def set_register(self, *args):
        """ 
            Dim Command 
            set register to value
            addr[short]:Address of register
            val[short]:Value to be set
            I:2
        """ 
        self._cmd("SET_REGISTER", *args)

    def enable_srclk(self, *args):
        """ 
            Dim Command 
            Set SRCLK
            B:1
        """ 
        self._cmd("ENABLE_SRCLK", *args)

    def send_n_triggers(self, *args):
        """ 
            Dim Command 
            Issue N software triggers (note that these are the triggers sent, not the triggers executed)
            N[int]: Number of triggers to be sent to the board.
            I
        """ 
        self._cmd("SEND_N_TRIGGERS", *args)

    def remove_slot(self, *args):
        """ 
            Dim Command 
            Remove the Iaddress in slot n. For a list see LIST
            slot[short]:Remove the address in slot n from the list
            S:1
        """ 
        self._cmd("REMOVE_SLOT", *args)

    def set_verbose(self, *args):
        """ 
            Dim Command 
            Set verbosity state
            verbosity[bool]:disable or enable verbosity for received data (yes/no), except dynamic data
            B:1
        """ 
        self._cmd("SET_VERBOSE", *args)

    def set_run_number(self, *args):
        """ 
            Dim Command 
            Sent a new run-number to the boards
            num[int]:Run number
            X:1
        """ 
        self._cmd("SET_RUN_NUMBER", *args)

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def enable_command_socket_mode(self, *args):
        """ 
            Dim Command 
            Set debug mode (yes: dump events through command socket, no=dump events through other sockets)
            B:1
        """ 
        self._cmd("ENABLE_COMMAND_SOCKET_MODE", *args)

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def raw_data(self):
        """ 
            Dim Service 
            DESC in SERVICE_DESC is empty ?!
            S:1;I:1;S:1;I:1;I:2;I:40;S:1440;S:160;F
        """ 
        return self._get("RAW_DATA")

    def list_slots(self, *args):
        """ 
            Dim Command 
            Print a list of all available board addressesa and whether they are enabled
            
        """ 
        self._cmd("LIST_SLOTS", *args)

    def set_debug_event_builder_out(self, *args):
        """ 
            Dim Command 
            Enable or disable the debug output from the event builder
            enable[bool]:Enable/Disable (yes/no)
            B:1
        """ 
        self._cmd("SET_DEBUG_EVENT_BUILDER_OUT", *args)

    def temperature(self):
        """ 
            Dim Service 
            FADs temperatures
            temp[deg. C]:0 global min, 1-40 min, 41 global max, 42-81 max
            F:82
        """ 
        return self._get("TEMPERATURE")

    def dump_stream(self, *args):
        """ 
            Dim Command 
            For debugging purpose: the binary data stream read from the sockets 0-7 can be dumped to files.
            switch[bool]:Enable (yes) or disable (no)
            B:1
        """ 
        self._cmd("DUMP_STREAM", *args)

    def region_of_interest(self):
        """ 
            Dim Service 
            DESC in SERVICE_DESC is empty ?!
            S:2
        """ 
        return self._get("REGION_OF_INTEREST")

    def ignore_events(self, *args):
        """ 
            Dim Command 
            Instructs the event-builder to ignore events from the given slot but still read the data from the socket.
            slot[short]:Slot from which the data should be ignored when building events
            enable[bool]:Whether the event builder should ignore data from this slot (yes) or allowed (no)
            S:1;B:1
        """ 
        self._cmd("IGNORE_EVENTS", *args)

    def start_run(self):
        """ 
            Dim Service 
            Run numbers
            run[idx]:Run no of last conf'd run (-1 if reset or none config'd yet)
            next[idx]:Run number which will be assigned to next configuration
            X:1;X:1
        """ 
        return self._get("START_RUN")

    def prescaler(self):
        """ 
            Dim Service 
            Trigger generator prescaler of fad boards
            prescaler[int]:Trigger generator prescaler value, for each board
            S:42
        """ 
        return self._get("PRESCALER")

    def file_format(self):
        """ 
            Dim Service 
            DESC in SERVICE_DESC is empty ?!
            S:1
        """ 
        return self._get("FILE_FORMAT")

    def runs(self):
        """ 
            Dim Service 
            Run files statistics
            stats[int]:num of open files, min/max run no, last opened or closed run
            file[string]:filename of last opened file
            I:5;C
        """ 
        return self._get("RUNS")

    def run_number(self):
        """ 
            Dim Service 
            Run numbers coming from FAD boards
            runNumbers[int]:current run number of each FAD board. 40=min, 41=max
            I:42
        """ 
        return self._get("RUN_NUMBER")

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def toggle(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            S:1
        """ 
        self._cmd("TOGGLE", *args)

    def reset_secondary_drs_baseline(self, *args):
        """ 
            Dim Command 
            DESC in SERVICE_DESC is empty ?!
            
        """ 
        self._cmd("RESET_SECONDARY_DRS_BASELINE", *args)

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def close_open_files(self, *args):
        """ 
            Dim Command 
            Close all run files opened by the EventBuilder.
            
        """ 
        self._cmd("CLOSE_OPEN_FILES", *args)

    def stats(self):
        """ 
            Dim Service 
            Statistics about size written
            X:4
        """ 
        return self._get("STATS")

    def soft_reset(self, *args):
        """ 
            Dim Command 
            Wait for buffers to drain, close all files and reinitialize event builder thread.
            
        """ 
        self._cmd("SOFT_RESET", *args)

    def hard_reset(self, *args):
        """ 
            Dim Command 
            Free all buffers, close all files and reinitialize event builder thread.
            
        """ 
        self._cmd("HARD_RESET", *args)

    def connections(self):
        """ 
            Dim Service 
            Connection status of FAD boards
            status[bitpattern]:lower bits stat1, upper bits stat2, for every board. 40=thread
            char[unknown]:to be completed
            C:40;C:1
        """ 
        return self._get("CONNECTIONS")

    def reset_configure(self, *args):
        """ 
            Dim Command 
            If configuration failed and the fadctrl is waiting for something, use this to reset the state.
            
        """ 
        self._cmd("RESET_CONFIGURE", *args)

class MAGIC_WEATHER ( FactDimServer): 
    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def data(self):
        """ 
            Dim Service 
            stat:Status
            T[deg C]:Temperature
            T_dew[deg C]:Dew point
            H[%]:Humidity
            P[hPa]:Air pressure
            v[km/h]:Wind speed
            v_max[km/h]:Wind gusts
            d[deg]:Wind direction (N-E)
            S:1;F:1;F:1;F:1;F:1;F:1;F:1;F:1
        """ 
        return self._get("DATA")

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def set_verbose(self, *args):
        """ 
            Dim Command 
            set verbosity state
            verbosity[bool]:disable or enable verbosity for received data (yes/no), except dynamic data
            B
        """ 
        self._cmd("SET_VERBOSE", *args)

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

class CHAT ( FactDimServer): 
    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def msg(self, *args):
        """ 
            Dim Command 
            msg[string]:message to be distributed
            C
        """ 
        self._cmd("MSG", *args)

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

class DRIVE_CONTROL ( FactDimServer): 
    def track_on(self, *args):
        """ 
            Dim Command 
            Move the telescope to the given position and start tracking
            Name[string]:Source name
            C
        """ 
        self._cmd("TRACK_ON", *args)

    def status(self):
        """ 
            Dim Service 
            DESC in SERVICE_DESC is empty ?!
            C:2;C:1
        """ 
        return self._get("STATUS")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def set_led_brightness(self, *args):
        """ 
            Dim Command 
            Set the LED brightness of the top and bottom leds
            top[au]:Allowed range 0-32767 for top LEDs
            bot[au]:Allowed range 0-32767 for bottom LEDs
            I:2
        """ 
        self._cmd("SET_LED_BRIGHTNESS", *args)

    def track_wobble(self, *args):
        """ 
            Dim Command 
            Move the telescope to the given wobble position around the given source and start tracking
            id:Wobble angle id (1 or 2)
            Name[string]:Source name
            S:1;C
        """ 
        self._cmd("TRACK_WOBBLE", *args)

    def wobble(self, *args):
        """ 
            Dim Command 
            Move the telescope to the given wobble position around the given sky coordinates and start tracking them
            Ra[h]:Right ascension
            Dec[deg]:Declination
            Offset[deg]:Wobble offset
            Angle[deg]:Wobble angle
            D:4
        """ 
        self._cmd("WOBBLE", *args)

    def source_position(self):
        """ 
            Dim Service 
            Ra_src[h]:Source right ascension
            Dec_src[deg]:Source declination
            Ra_cmd[h]:Command right ascension
            Dec_cmd[deg]:Command declination
            Offset[deg]:Wobble offset
            Angle[deg]:Wobble angle
            Name[string]:Source name if available
            D:1;D:1;D:1;D:1;D:1;D:1;C:31
        """ 
        return self._get("SOURCE_POSITION")

    def move_to(self, *args):
        """ 
            Dim Command 
            Move the telescope to the given local coordinates
            Zd[deg]:Zenith distance
            Az[deg]:Azimuth
            D:2
        """ 
        self._cmd("MOVE_TO", *args)

    def stop(self, *args):
        """ 
            Dim Command 
            Stop any kind of movement.
            
        """ 
        self._cmd("STOP", *args)

    def leds_off(self, *args):
        """ 
            Dim Command 
            Switch off TPoint LEDs
            
        """ 
        self._cmd("LEDS_OFF", *args)

    def jupiter(self, *args):
        """ 
            Dim Command 
            Start tracking Jupiter
            
        """ 
        self._cmd("JUPITER", *args)

    def park(self, *args):
        """ 
            Dim Command 
            Park the telescope
            
        """ 
        self._cmd("PARK", *args)

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def moon(self, *args):
        """ 
            Dim Command 
            Start tracking the moon
            
        """ 
        self._cmd("MOON", *args)

    def reload_sources(self, *args):
        """ 
            Dim Command 
            Reload sources from database after database has changed..
            
        """ 
        self._cmd("RELOAD_SOURCES", *args)

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def print_cmd(self, *args):
        """ 
            Dim Command 
            Print source list.
            
        """ 
        self._cmd("PRINT_CMD", *args)

    def set_verbose(self, *args):
        """ 
            Dim Command 
            Set verbosity state
            verbosity[bool]:disable or enable verbosity for received data (yes/no), except dynamic data
            B
        """ 
        self._cmd("SET_VERBOSE", *args)

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def resume(self, *args):
        """ 
            Dim Command 
            If drive is in Error state, this can b used to resume the last tracking command, if the last command sent to cosy was a tracking command.
            
        """ 
        self._cmd("RESUME", *args)

    def tpoint(self):
        """ 
            Dim Service 
            Take a TPoint (given values will be written to the TPoint files)
            mag[float]:Magnitude of the star
            name[string]:Name of the star
            D:1;D:1;D:1;D:1;D:1;D:1;D:1;D:1;S:1;D:1;D:1;D:1;D:1;D:1;D:1;D:1;C
        """ 
        return self._get("TPOINT")

    def set_autoresume(self, *args):
        """ 
            Dim Command 
            Enable/disable auto resume
            resume[bool]:if enabled, drive is tracking and goes to error state, the last tracking command is repeated automatically.
            B
        """ 
        self._cmd("SET_AUTORESUME", *args)

    def reconnect(self, *args):
        """ 
            Dim Command 
            (Re)connect ethernet connection to FTM, a new address can be given
            [host][string]:new ethernet address in the form <host:port>
            O
        """ 
        self._cmd("RECONNECT", *args)

    def tracking_position(self):
        """ 
            Dim Service 
            Ra[h]:Command right ascension
            Dec[deg]:Command declination
            Ha[h]:Corresponding hour angle
            Zd[deg]:Nominal zenith distance
            Az[deg]:Nominal azimuth angle
            dZd[deg]:Control deviation Zd
            dAz[deg]:Control deviation Az
            dev[arcsec]:Absolute control deviation
            D:1;D:1;D:1;D:1;D:1;D:1;D:1;D:1
        """ 
        return self._get("TRACKING_POSITION")

    def venus(self, *args):
        """ 
            Dim Command 
            Start tracking Venus
            
        """ 
        self._cmd("VENUS", *args)

    def take_tpoint(self, *args):
        """ 
            Dim Command 
            Take a TPoint
            
        """ 
        self._cmd("TAKE_TPOINT", *args)

    def disconnect(self, *args):
        """ 
            Dim Command 
            disconnect from ethernet
            
        """ 
        self._cmd("DISCONNECT", *args)

    def saturn(self, *args):
        """ 
            Dim Command 
            Start tracking Saturn
            
        """ 
        self._cmd("SATURN", *args)

    def unlock(self, *args):
        """ 
            Dim Command 
            Unlock locked state.
            
        """ 
        self._cmd("UNLOCK", *args)

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def pointing_position(self):
        """ 
            Dim Service 
            Zd[deg]:Zenith distance (encoder readout)
            Az[deg]:Azimuth angle (encoder readout)
            D:1;D:1
        """ 
        return self._get("POINTING_POSITION")

    def track(self, *args):
        """ 
            Dim Command 
            Move the telescope to the given sky coordinates and start tracking them
            Ra[h]:Right ascension
            Dec[deg]:Declination
            D:2
        """ 
        self._cmd("TRACK", *args)

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def mars(self, *args):
        """ 
            Dim Command 
            Start tracking Mars
            
        """ 
        self._cmd("MARS", *args)

    def track_source(self, *args):
        """ 
            Dim Command 
            Move the telescope to the given wobble position around the given source and start tracking
            Offset[deg]:Wobble offset
            Angle[deg]:Wobble angle
            Name[string]:Source name
            D:2;C
        """ 
        self._cmd("TRACK_SOURCE", *args)

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

class SMART_FACT ( FactDimServer): 
    def print_cmd(self, *args):
        """ 
            Dim Command 
            Print a list of the states of all connected servers.
            
        """ 
        self._cmd("PRINT_CMD", *args)

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

class MCP ( FactDimServer): 
    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def print_cmd(self, *args):
        """ 
            Dim Command 
            Print the states and connection status of all systems connected to the MCP.
            
        """ 
        self._cmd("PRINT_CMD", *args)

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def stop(self, *args):
        """ 
            Dim Command 
            Stops the trigger (either disables the FTM trigger or the internal DRS trigger)
            
        """ 
        self._cmd("STOP", *args)

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def start(self, *args):
        """ 
            Dim Command 
            Start the configuration and data taking for a run-type of a pre-defined setup
            TimeMax[s]:Maximum number of seconds before the run will be closed automatically
            NumMax[count]:Maximum number events before the run will be closed automatically
            Name[text]:Name of the configuration to be used for taking data
            X:2;C
        """ 
        self._cmd("START", *args)

    def reset(self, *args):
        """ 
            Dim Command 
            If a configuration blockes because a system cannot configure itself properly, this command can be called to leave the configuration procedure. The command is also propagated to FTM and FAD
            
        """ 
        self._cmd("RESET", *args)

    def configuration(self):
        """ 
            Dim Service 
            Run configuration information
            MaxTime[s]:Maximum time before the run gets stopped
            MaxEvents[num]:Maximum number of events before the run gets stopped
            Name[text]:Name of the chosen configuration
            X:1;X:1;C
        """ 
        return self._get("CONFIGURATION")

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

class TIME_CHECK ( FactDimServer): 
    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

class BIAS_CONTROL ( FactDimServer): 
    def dac(self):
        """ 
            Dim Service 
            U[dac]:Current dac setting
            Uref[dac]:Reference dac setting
            S:416;S:416
        """ 
        return self._get("DAC")

    def set_channel_dac(self, *args):
        """ 
            Dim Command 
            Set a new target value in DAC counts for a single channel. Starts ramping if necessary.
            channel[short]:Channel for which to set the target voltage [0-415]
            voltage[dac]:Target voltage in DAC units for the given channel
            S:1;S:1
        """ 
        self._cmd("SET_CHANNEL_DAC", *args)

    def request_status(self, *args):
        """ 
            Dim Command 
            Asynchronously request the status (current) of all channels.
            
        """ 
        self._cmd("REQUEST_STATUS", *args)

    def expert_reset(self, *args):
        """ 
            Dim Command 
            Send the RESET command (note that this is possibly harmfull command)
            
        """ 
        self._cmd("EXPERT_RESET", *args)

    def increase_all_channels_voltage(self, *args):
        """ 
            Dim Command 
            Add the given voltages to the current reference voltages. Starts ramping if necessary.offset[V]:Offsets to be added to the reference voltage of all channels in volts
            F:416
        """ 
        self._cmd("INCREASE_ALL_CHANNELS_VOLTAGE", *args)

    def reset_over_current_status(self, *args):
        """ 
            Dim Command 
            Set all channels in over current state to 0V and send a system reset to reset the over current flags.
            
        """ 
        self._cmd("RESET_OVER_CURRENT_STATUS", *args)

    def print_currents(self, *args):
        """ 
            Dim Command 
            Print a table with all current read back with the last request operation
            
        """ 
        self._cmd("PRINT_CURRENTS", *args)

    def print_info(self, *args):
        """ 
            Dim Command 
            Print a table with all current read back with the last request operation
            
        """ 
        self._cmd("PRINT_INFO", *args)

    def reconnect(self, *args):
        """ 
            Dim Command 
            (Re)connect USB connection to Bias power supply, a new address can be given
            tty[string]:new USB address
            O
        """ 
        self._cmd("RECONNECT", *args)

    def enable_dummy_mode(self, *args):
        """ 
            Dim Command 
            Enable dummy mode. In this mode SetAllChannels prints informations instead of sending anything to the bias crate.
            enable[bool]:disable or enable dummy mode
            B:1
        """ 
        self._cmd("ENABLE_DUMMY_MODE", *args)

    def set_channel_offset_to_zero(self, *args):
        """ 
            Dim Command 
            Set a single channel channels to its G-APD reference voltage. Starts ramping if necessary.
            channel[short]:Channel for which to set the target voltage [0-416]
            S:1
        """ 
        self._cmd("SET_CHANNEL_OFFSET_TO_ZERO", *args)

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def set_channel_voltage(self, *args):
        """ 
            Dim Command 
            Set a new target voltage for a single channel. Starts ramping if necessary.
            channel[short]:Channel for which to set the target voltage [0-415]
            voltage[V]:Target voltage in volts for the given channel (will be converted to DAC units)
            S:1;F:1
        """ 
        self._cmd("SET_CHANNEL_VOLTAGE", *args)

    def set_global_dac(self, *args):
        """ 
            Dim Command 
            Set a new target value for all channels in DAC counts. Starts ramping if necessary. (This command is not realized with the GLOBAL SET command.)
            voltage[dac]:Global target voltage in DAC counts.
            S:1
        """ 
        self._cmd("SET_GLOBAL_DAC", *args)

    def set_all_channels_voltage(self, *args):
        """ 
            Dim Command 
            Set all channels to the given new reference voltage. Starts ramping if necessary.voltage[V]:New reference voltage for all channels
            F:416
        """ 
        self._cmd("SET_ALL_CHANNELS_VOLTAGE", *args)

    def voltage(self):
        """ 
            Dim Service 
            Uout[V]:Output voltage
            F:416
        """ 
        return self._get("VOLTAGE")

    def stop(self, *args):
        """ 
            Dim Command 
            Stop an on-going ramping
            
        """ 
        self._cmd("STOP", *args)

    def expert_set_channel_voltage(self, *args):
        """ 
            Dim Command 
            Send a single channel set command. The given voltage is converted to DAC commands.
            S:1;F:1
        """ 
        self._cmd("EXPERT_SET_CHANNEL_VOLTAGE", *args)

    def set_zero_voltage(self, *args):
        """ 
            Dim Command 
            Set all channels to a zero reference voltage. Starts ramping if necessary.
            
        """ 
        self._cmd("SET_ZERO_VOLTAGE", *args)

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def print_voltages(self, *args):
        """ 
            Dim Command 
            Print a table with all voltages (current and reference voltages as currently in memory)
            
        """ 
        self._cmd("PRINT_VOLTAGES", *args)

    def increase_global_voltage(self, *args):
        """ 
            Dim Command 
            Increases the voltage of all channels by the given offset. Starts ramping if necessary. (This command is not realized with the GLOBAL SET command.)
            offset[V]:Offset to be added to all channels (will be converted to DAC counts)
            F:1
        """ 
        self._cmd("INCREASE_GLOBAL_VOLTAGE", *args)

    def set_global_voltage(self, *args):
        """ 
            Dim Command 
            Set a new target voltage for all channels. Starts ramping if necessary. (This command is not realized with the GLOBAL SET command.)
            voltage[V]:Global target voltage in volts (will be converted to DAC units)
            F:1
        """ 
        self._cmd("SET_GLOBAL_VOLTAGE", *args)

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def expert_set_global_dac(self, *args):
        """ 
            Dim Command 
            Send the global set command.
            S:1
        """ 
        self._cmd("EXPERT_SET_GLOBAL_DAC", *args)

    def nominal(self):
        """ 
            Dim Service 
            Ubr[V]:Nominal breakdown voltage at 25deg C
            Uov[V]:Nominal overvoltage
            Uoff[V]:Bias crate channel offsets
            F:416;F:416;F:416
        """ 
        return self._get("NOMINAL")

    def set_update_interval(self, *args):
        """ 
            Dim Command 
            Set the updat einterval how often the currents are requested
            interval[ms]:Update interval in milliseconds
            I:1
        """ 
        self._cmd("SET_UPDATE_INTERVAL", *args)

    def expert_set_channel_dac(self, *args):
        """ 
            Dim Command 
            Send a single channel set command.
            S:1;S:1
        """ 
        self._cmd("EXPERT_SET_CHANNEL_DAC", *args)

    def increase_channel_voltage(self, *args):
        """ 
            Dim Command 
            Increases the voltage of all channels by the given offset. Starts ramping if necessary. (This command is not realized with the GLOBAL SET command.)
            channel[short]:Channel for which to adapt the voltage [0-415]
            offset[V]:Offset to be added to all channels (will be converted to DAC counts)
            S:1;F:1
        """ 
        self._cmd("INCREASE_CHANNEL_VOLTAGE", *args)

    def set_all_channels_offset(self, *args):
        """ 
            Dim Command 
            Set all channels to their G-APD reference voltage plus the given offset. Starts ramping if necessary.
            offset[V]:Offset to be added to teh G-APD reference voltage globally
            F:416
        """ 
        self._cmd("SET_ALL_CHANNELS_OFFSET", *args)

    def set_global_offset_to_zero(self, *args):
        """ 
            Dim Command 
            Set all channels to their G-APD reference voltage. Starts ramping if necessary.
            
        """ 
        self._cmd("SET_GLOBAL_OFFSET_TO_ZERO", *args)

    def set_global_offset(self, *args):
        """ 
            Dim Command 
            Set all channels to their G-APD breakdown voltage plus overvoltage plus the given offset. Starts ramping if necessary.
            offset[V]:Offset to be added to the G-APD reference voltage globally
            F:1
        """ 
        self._cmd("SET_GLOBAL_OFFSET", *args)

    def current(self):
        """ 
            Dim Service 
            I[dac]:Bias current (conversion: 5000uA/4096dac)
            S:416
        """ 
        return self._get("CURRENT")

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def print_gapd_reference_voltages(self, *args):
        """ 
            Dim Command 
            Print the G-APD reference values (breakdown voltage + overvoltage) obtained from file
            
        """ 
        self._cmd("PRINT_GAPD_REFERENCE_VOLTAGES", *args)

    def set_verbose(self, *args):
        """ 
            Dim Command 
            set verbosity state
            verbosity[bool]:disable or enable verbosity for received data (yes/no), except dynamic data
            B:1
        """ 
        self._cmd("SET_VERBOSE", *args)

    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def expert_mode(self, *args):
        """ 
            Dim Command 
            Enable usage of expert commands (note that for safty reasons the are exclusive with the standard commands)
            B:1
        """ 
        self._cmd("EXPERT_MODE", *args)

    def expert_set_global_voltage(self, *args):
        """ 
            Dim Command 
            Send the global set command. The given voltage is converted to DAC counts.
            F:1
        """ 
        self._cmd("EXPERT_SET_GLOBAL_VOLTAGE", *args)

    def set_channel_offset(self, *args):
        """ 
            Dim Command 
            Set single channels to its G-APD breakdown voltage plus overvoltage plus the given offset. Starts ramping if necessary.
            channel[short]:Channel for which to set the target voltage [0-415]
            offset[V]:Offset to be added to the G-APD reference voltage of the given channel
            S:1;F:1
        """ 
        self._cmd("SET_CHANNEL_OFFSET", *args)

    def start(self, *args):
        """ 
            Dim Command 
            Start a ramping if no ramping is in progress and if reference values differ from current voltages
            
        """ 
        self._cmd("START", *args)

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

    def disconnect(self, *args):
        """ 
            Dim Command 
            disconnect from USB
            
        """ 
        self._cmd("DISCONNECT", *args)

class DIM_CONTROL ( FactDimServer): 
    def service_list(self):
        """ 
            Dim Service 
            Native Dim service: List of services, commands and formats
            ServiceList[string]:For details see the Dim manual.
            C
        """ 
        return self._get("SERVICE_LIST")

    def message(self):
        """ 
            Dim Service 
            A general logging service providing a quality of service (severity)
            Message[string]:The message
            C
        """ 
        return self._get("MESSAGE")

    def start(self, *args):
        """ 
            Dim Command 
            Start a JavaScript
            C
        """ 
        self._cmd("START", *args)

    def stop(self, *args):
        """ 
            Dim Command 
            Stop a runnning batch script or JavaScript
            C
        """ 
        self._cmd("STOP", *args)

    def state(self):
        """ 
            Dim Service 
            Provides the state of the state machine as quality of service.
            Text[string]:A human readable string sent by the last state change.
            C
        """ 
        return self._get("STATE")

    def client_list(self):
        """ 
            Dim Service 
            Native Dim service: A list of all connected clients
            C
        """ 
        return self._get("CLIENT_LIST")

    def exit(self, *args):
        """ 
            Dim Command 
            This is a native Dim command: Exit programremotely. FACT++ programs use the given number as return code.
            Rc[int]:Return code, under normal circumstances this should be 0 or 1 (42 will call exit() directly, 0x42 will call abort() directly.
            L:1
        """ 
        self._cmd("EXIT", *args)

    def execute(self, *args):
        """ 
            Dim Command 
            Execute a batch script
            C
        """ 
        self._cmd("EXECUTE", *args)

    def service_desc(self):
        """ 
            Dim Service 
            Descriptions of services or commands and there arguments
            Description[string]:For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.
            C
        """ 
        return self._get("SERVICE_DESC")

    def state_list(self):
        """ 
            Dim Service 
            Provides a list with descriptions for each service.
            StateList[string]:A \n separated list of the form id:name=description
            C
        """ 
        return self._get("STATE_LIST")

    def version_number(self):
        """ 
            Dim Service 
            Native Dim service: Version number of Dim in use
            DimVer[int]:Version*100+Release (e.g. V19r17 = 1917)
            L
        """ 
        return self._get("VERSION_NUMBER")

