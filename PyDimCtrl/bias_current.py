#!/usr/bin/python -tti
from factdimserver import *
import numpy as np
import sys

cali = feedback.calibration()
avg = np.array(cali[0:320])
rms = np.array(cali[416:416+320])
r = np.array(cali[2*416:2*416+320])

bias = bias_control

I = np.array(bias.current()[0:320], dtype=float)
II = I/4096. * 5000
V = np.array(bias.voltage()[0:320])
if len(sys.argv) > 1:
    i = int(sys.argv[1])
else: i=0
print 'I:', I[i], 'dac\t', II[i], 'uA'
print 'V:', V[i]
print ''
print 'GUI offset:', V[i]/r[i]*1e6
print 'GUI feedback:', II[0] - V[0]/r[0]*1e6

GUII = II-V/r*1e6

print 'median', np.median(GUII)
print 'mean', GUII.mean()
print 'rms', ((GUII- GUII.mean())**2).sum()
print 'std', GUII.std()
print 'max', GUII.max()
print 'min', GUII.min()
