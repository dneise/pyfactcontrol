#!/usr/bin/python -tti

from pylab import *
import numpy as np
from scipy import *
from matplotlib.patches import RegularPolygon
import time

from factdimserver import *


class EventBrowser:
    
    def __init__(self, filename):
        
        self.LoadDetector("")

        self.fig = figure(figsize =(12, 5))
        self.ax = self.fig.add_subplot(122)
        self.ax2 = self.fig.add_subplot(121)
        self.ax.set_axis_off()
        self.axz, kk = mpl.colorbar.make_axes(self.ax, orientation='vertical')
        self.fig.canvas.mpl_connect('pick_event', self.onpick)
        self.ww = np.random.rand(1440)


        self.Plot()
        self.timer = self.fig.canvas.new_timer(interval=10000)
        self.timer.add_callback(self.timer_callback)
        self.timer.start()

    def timer_callback(self):
        self.adc_data = get_fad_adc()
        self.cam_data = get_fad_cams()
        self.ww = cam_data[0]
        self.Plot()
       

    def LoadDetector(self,filename):
        if filename == "":
            filename = "FACTmap111030.txt"
        file = open(filename)
        xpixel = []
        xpixel = []
        ypixel = []
        idpixel = []

        for lines in file:
            if lines[0] != '#':
                softid = int(lines.split()[0])
                idpixel.append(softid)
                x = -1*float(lines.split()[9])
                y = float(lines.split()[8])
                if softid == 1438 or softid == 1439:
                    x = -1 * x
                xpixel.append(x)
                ypixel.append(y)


        self.xpixel = xpixel
        self.ypixel = ypixel
        self.idpixel = idpixel

    def Plot(self):
        ww = self.ww
        wmax = ww.max()
        ww /= ww.max()
        cmap = plt.cm.spectral
        
        for x, y, w in zip(self.xpixel, self.ypixel, ww):
            self.ax.add_artist(RegularPolygon([x, y], 6, 0.6, 0, facecolor=cmap(w),edgecolor='black',linewidth=0.2))
        self.ax.plot(self.xpixel, self.ypixel, 'h', color="white",  ms=12, visible=False, picker=5)
        self.ax.set_xlim(-21, 21)
        self.ax.set_ylim(-21, 21)
        self.axz.cla()

        #Create axes for the color bar
        norm = mpl.colors.Normalize(vmin=0, vmax=wmax)
        cb1 = mpl.colorbar.ColorbarBase(self.axz, cmap=cmap,
                                        norm=norm,
                                        orientation='vertical')
#        text = "Event Browser"
#        self.ax.annotate(text, (0.05, 0.05),
#                         xycoords="figure fraction", va="center", ha="left",
#                         bbox=dict(boxstyle="round, pad=1", fc="w"))

        
    def onpress(self, event):
        'define some key press events'
        if event.key in ('q','Q'): sys.exit()
        if event.key not in ('n', 'p'): return
        if event.key=='n': 
            self.event = self.run.next()
        else:  
            print "Sorry I cannot go back!"
            return
        self.ax.cla()
        self.ax.set_axis_off()
        self.Plot()

    def onpick(self, event):
        xpix = self.xpixel
        ypix = self.ypixel
        # the click locations
        x = event.mouseevent.xdata
        y = event.mouseevent.ydata
        ind = event.ind

        N = len(event.ind)
        if not N: return True
        
        if N == 1:
            self.pixelID = event.ind[0]
        if N > 1:
            # find nearest point to click
            distances = np.zeros(N)
            for ii,i in enumerate(ind):
                distances[ii] = np.sqrt((x-xpix[i])**2 + (y-ypix[i])**2)
            self.pixelID = ind[distances.argmin()]
            
        print self.pixelID
        
        self.ax2.plot(self.adc_data[self.pixelID], hold=False)
        
        self.fig.canvas.draw()



# *blocking* function to get the FAD ADC data in a nice format
def get_fad_adc():
    # a large large tuple:
    # 1650 elements carrying header information
    offset = 1650
    # (1440+160)*Roi floats of real ADC data (DRS calibrated, 
    #    if fad_control has the constants)
    raw_data = fad_control.raw_data()
    if raw_data == None:
        print "fad_control.raw_data() .. timed out"
        return None
    Roi = int(raw_data[0])
    rd = np.array(raw_data[offset:offset+1440*Roi])
    rd = raw_data.reshape(1440,Roi)
    return rd


def get_fad_cams():

    cd = fad_control.event_data()
    if cd == None:
        print "fad_control.event_data() ... timed out"
        return None
    cd = np.array(cd)
    cd = cd.reshape(4,1440)

    return cd



if __name__ == '__main__':
    eb = EventBrowser("")
    show()
    
    
   

    
      
  
      
