#!/usr/bin/python -tti

from pylab import *
import numpy as np
from scipy import *
from matplotlib.patches import RegularPolygon
from factdimserver import *


class EventBrowser:
    
    def __init__(self, run, filename):
        
        self.LoadDetector("")
        self.run = run
        self.event = run.next()
        self.nevents = run.nevents
        self.fig = figure(figsize =(10, 8))
        self.ax = self.fig.add_subplot(111)
        self.ax.set_axis_off()
        self.axz, kk = mpl.colorbar.make_axes(self.ax, orientation='vertical')
        self.Plot()
        # Create a new timer object. Set the interval 1000 milliseconds (1000 is default)
        # and tell the timer what function should be called.
        self.timer = fig.canvas.new_timer(interval=1000)
        self.timer.add_callback(self.timer_callback)
        self.timer.start()
        self.fig.canvas.mpl_connect('pick_event', self.onpick)
        self.fig.canvas.mpl_connect('key_press_event', self.onpress)
       
    def timer_callback(self):
        self.adc_data = get_fad_adc()
        self.cam_data = get_fad_cams()

    def LoadDetector(self,filename):
        if filename == "":
            filename  = "FACTmap111030.txt"
        file = open(filename)
        xpixel = []
        ypixel = []
        idpixel = []

        for lines in file:
            if lines[0] != '#':
                softid = int(lines.split()[0])
                idpixel.append(softid)
                x = -1*float(lines.split()[9])
                y = float(lines.split()[8])
                if softid == 1438 or softid == 1439:
                    x = -1 * x
                xpixel.append(x)
                ypixel.append(y)


        self.xpixel = xpixel
        self.ypixel = ypixel
        self.idpixel = idpixel

    def Plot(self):
        ww, dummy = self.max_finder(self.despiker(self.event['data']))
        print ww
        self.ww = ww
        wmax = ww.max()
        cmap = plt.cm.spectral
        for x, y, w in zip(self.xpixel, self.ypixel, ww):
            self.ax.add_artist(RegularPolygon([x, y], 6, 0.6, 0, facecolor=cmap(w),edgecolor='black',linewidth=0.2))
        self.ax.plot(self.xpixel, self.ypixel, 'h', color="white",  ms=12, visible=False, picker=10)
        self.ax.set_xlim(-21, 21)
        self.ax.set_ylim(-21, 21)
        self.axz.cla()

        #Create axes for the color bar
        norm = mpl.colors.Normalize(vmin=0, vmax=wmax)
        cb1 = mpl.colorbar.ColorbarBase(self.axz, cmap=cmap,
                                        norm=norm,
                                        orientation='vertical')
        text = "Event Browser, press 'n' in this window to iterate over the events."
        text = text + "\nEvent id: " + str(self.event["event_id"])
        text = text + "\nTrigger type: " + str(self.event["trigger_type"])
        self.ax.annotate(text, (0.05, 0.05),
                         xycoords="figure fraction", va="center", ha="left",
                         bbox=dict(boxstyle="round, pad=1", fc="w"))
        
    def onpress(self, event):
        'define some key press events'
        if event.key in ('q','Q'): sys.exit()
        if event.key not in ('n', 'p'): return
        if event.key=='n': 
            self.event = self.run.next()
        else:  
            print "Sorry I cannot go back!"
            return
        self.ax.cla()
        self.ax.set_axis_off()
        self.Plot()
        self.update()

    def onpick(self, event):
        
        N = len(event.ind)
        if not N: return True
        
        if N > 1:
            print '%i points found!' % N
        self.pixelID = event.ind[0]
        
        # the click locations
        x = event.mouseevent.xdata
        y = event.mouseevent.ydata
        print x, y
        self.update()

    
    def update(self):
        
        dataind = self.pixelID
        
        # put a user function in here!        
        self.userfunc(dataind)

        self.fig.canvas.draw()
        show()
        
    def userfunc(self,dataind):
        print 'No userfunc defined'
        pass


# *blocking* function to get the FAD ADC data in a nice format
def get_fad_adc():
    # a large large tuple:
    # 1650 elements carrying header information
    offset = 1650
    # (1440+160)*Roi floats of real ADC data (DRS calibrated, 
    #    if fad_control has the constants)
    raw_data = fad_control.raw_data()
    if raw_data == None:
        print "fad_control.raw_data() .. timed out"
        return None
    Roi = int(raw_data[0])
    rd = np.array(raw_data[offset:offset+1440*Roi])
    rd = raw_data.reshape(1440,Roi)
    return rd


def get_fad_cams():

    cd = fad_control.event_data()
    if cd == None:
        print "fad_control.event_data() ... timed out"
        return None
    cd = np.array(cd)
    cd = cd.reshape(1440,4)

    return cd

if __name__ == '__main__':
    

    eb = EventBrowser(run, "")
   
    def PlotFFT(pixelID):
        
        fig2 = figure(2, figsize=(10, 8))                
        
        ax1= fig2.add_subplot(311)
        ax2 = fig2.add_subplot(312)
        ax3 = fig2.add_subplot(313, sharey=ax1, sharex=ax1)
        
        ax1.cla()
        ax2.cla()
        ax3.cla()
        
        ax1.set_title("PixelID "+str(pixelID))

        ax1.grid()
        ax2.grid()
        ax3.grid()
        
        x_drs = np.arange(0, 300, 1)

        truncated_x_drs = x_drs[20:-10]
        
        data = eb.event['data']
        
        drsdata = data[pixelID] 
        ax1.plot(x_drs, drsdata, '.:')
        
        step = zeros(300)
        step[:100] = 1
        ft = fft(drsdata)
        
        new_drs = np.arange(-150, 150, 1)
        
        ftdom = ft.copy()
        for i in range(10,290):
            ftdom[i]=0
        ax2.plot(new_drs, abs(concatenate((ftdom[150:],ftdom[:150]))), marker='o')
        invdata = ifft(ftdom)
        ax3.plot(x_drs, invdata, '.:')
        ax3.plot(x_drs, drsdata, '.:')
        ax3.set_xlabel("DRS counts")
        fig2.canvas.draw()
        
         
    eb.userfunc = PlotFFT
    
    show()
   

    
      
  
      
