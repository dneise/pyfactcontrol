#!/bin/env python
# -*- coding: UTF-8 -*-
"""
An example for showing how to create a FACT++ server

The server exposes two services that are updated periodically. One of them uses
its function callback to get a string with the current time, the other one
returns a pair of numeric values.

"""

import sys
import time
import pydim
import threading
class MyFactppLikeServer(threading.Thread):
    """
    A simple DIM server with two services, that provides also the additional
    services defined by the FACT++ dim service std.
    """

    def __init__(self, name, dns_node=None):
        threading.Thread.__init__(self)
        self.name = name.upper()
        self._stop = False
        try:
            if dns_node is None:
                # raises RuntimeError, in case the node is not found
                pydim.dis_get_dns_node()
            else:
                # raises RuntimeError in case the dns_node string
                # does not refer to a valid Dim Dns node
                pydim.dis_set_dns_node(dns_node)
        except RuntimeError as e:
            print "No Dim DNS node found. Please set the environment variable DIM_DNS_NODE"
            raise

        # I want to collect all services in a dict.
        self.services = {}

        self.services['example1'] = pydim.dis_add_service(self.name+"/EXAMPLE1", "C", self.service_callback, 0)
        self.services['example2'] = pydim.dis_add_service(self.name+"/EXAMPLE2", "F:1;I:1;", self.service_callback2, 0)
        self.services['service_desc'] = pydim.dis_add_service(self.name+"/SERVICE_DESC", "C", self.service_desc_callback, 0)
        self.services['state_list'] = pydim.dis_add_service(self.name+"/STATE_LIST", "C", self.state_list_callback, 0)
        self.services['message'] = pydim.dis_add_service(self.name+"/MESSAGE", "C", self.message_callback, 0)
        self.services['state'] = pydim.dis_add_service(self.name+"/STATE", "C", self.state_callback, 0)

        for service_name, service_id in self.services.items():
            if not service_id:
                raise Exception("An error occurred while registering the service")
            pydim.dis_update_service(service_id)

        pydim.dis_start_serving(self.name)

    def service_callback(self, tag):
        """
        Service callbacks are functions (in general, Python callable objects)
        that take one argument: the DIM tag used when the service was added,
        and returns a tuple with the values that corresponds to the service
        parameters definition in DIM.
        """

        print "Running callback function for service 1"

        # Calculate the value
        # This example returns a string with the current time
        now = time.strftime("%X")

        # Remember, the callback function must return a tuple
        return ("Hello! The time is %s" % now,)

    def service_callback2(self, tag):
        """
        The callback function for the second service.
        """
        val1 = 3.11
        val2 = 42
        print "Running callback function for service 2: val1={0}, val2={1}".format(val1, val2)
        return (val1, val2)

    def service_desc_callback(self, tag):
        """
        This callback function provides the service description string as
        requested by the FACT++ dim service std.
        The string should never change during the lifetime of a server, since
        it is highly unlikely for a server to change its services.

        The format of a service description is:
        SVR_NAME/SVC_NAME=general description|PAR_NAME[type]:parameter description\n

        The 2nd part from PAR_NAME to "parameter descripter" may occur repeatedly,
        delimited by '|' characters.

        Each service description is delimited from the next by the '\n' character.
        The entire string should be '\0' terminated, as usual in the C-world.
        """

        # keep in mind we have to return a tuple, containing a single string
        return (
            self.name+"/SERVICE_LIST=\n"
            +self.name+"/CLIENT_LIST=\n"
            +self.name+"/VERSION_NUMBER=\n"
            +self.name+"/EXIT=\n"
            +self.name+"/SET_EXIT_HANDLER=\n"
            +self.name+"/SERVICE_DESC=describes all services|description[string]:contains the description\n"
            +self.name+"/STATE=the state of the server|state[string]:the state_name and the reason for the state in a textual form\n"
            +self.name+"/STATE_LIST=all states of the server|stateDescription[string]:a verbose description of all possible states of the server.\n"
            +self.name+"/MESSAGE=a general log message|message[string]:the last message of the server\n"
            +self.name+"/EXAMPLE1=the time as a string|time[string]:the local time of the server as a string\n"
            +self.name+"/EXAMPLE2=two floats|val1[float]: some arbitrary value|val2[float]: another, but also arbitrary value\n"
            +"\0", )

    def state_list_callback(self, tag):
        """
        state list format:
        signed integer:state_name=state description\n

        state list ends with '\0'
        """
        return (
            "-2:Unknown=The state is not known\n"
            "-1:NotReady=we are not ready\n"
            "0:Ready=we are ready\n"
            "1:thinking=We are currently thinking\n"
            "256:ERROR=Common error state\n"
            "65535:FATAL=a fatal error occured\n"
            "\0", )

    def message_callback(self, tag):
        """
        I don't know the format of the message string, but an Example
        looks like this.

        "State Transition from Connected[2] to VoltageOn[5] (by Run())\0"
        """
        try:
            msg = str(self.message)
            if msg[-1] != '\0':
                msg += '\0'
        except AttributeError:
            msg = "No Message avaliable.\0"
        return (msg, )

    def state_callback(self, tag):
        """
        I don't know the format of the State String, but an Example looks like this:
        "DriveOff[12] by Run()\0"

        """
        try:
            state_name = str(self.state_name)
            state_id = int(self.state_id)
            state_string = state_name+"[{}]".format(state_id)+"\0"
        except Exception:
            state_string = "Unknown[-2] I don't know why.\0"
        return (state_string, )



    def run(self):
        # Initial values for the service 2. Please see below.
        val1 = 3.11
        val2 = 0

        while True:
            # Update the services periodically (each 5 seconds)
            time.sleep(5)
            print ""

            # Case 1: When `dis_update_service` is called without arguments the
            # callback function will be executed and its return value
            # will be sent to the clients.
            print "Updating the service 1 with the callback function"
            pydim.dis_update_service(self.services['example1'])


            # Case 2: When `dis_update_service` is called with arguments, they are
            # sent directly to the clients as the service value, *without* executing the
            # callback function. Please note that the number and the type of the
            # arguments must correspond to the service description.
            #

            # Update the second server each 10 seconds
            #
            if val2 % 2:
                print "Updating the service 2 with direct values"
                pydim.dis_update_service(self.services['example2'], (val1, val2))

            # For the sake of the example, update the values passed to svc2:
            val1 = val1 + 11.30
            val2 = val2 + 1

            if self._stop:
                print "self.top is True .. . stopping"
                break

    def stop(self):
        print "stop() called"
        self._stop = True



if __name__ == "__main__":
    s = MyFactppLikeServer(name="peter")
    s.start()
    s.join()
