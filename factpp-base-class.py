#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
An example for a FACT++ base class, that may be used for inheriting the
FACT++ std services.
"""

import sys
import pydim
import threading

class States(object):
    """ has a couple of States inside
    """
    pass


class State(object):
    def __init__(self, name, id, text=""):
        self.name = name
        self.id = id
        self.text = text


class Service(object):
    def __init__(self,
                    name,
                    format,
                    callback,
                    tag=0):
        self.name = name.upper()
        self.format = format
        #TODO: check if callback is callable, takes exactly one argument
        # and returns the correct format?.
        # No this is done by pydim.
        self.callback = callback
        self.tag = tag

        # ID is filled after pydim.dis_add_service was called
        self.id = None

        self.description = ""
        self.parameter_description = {}


class FactppBase(object):

    def __init__(self, name, dns_node=None):

        self.name = name.upper()
        self._auto_update_services=['state']

        try:
            if dns_node is None:
                # raises RuntimeError, in case the node is not found
                pydim.dis_get_dns_node()
            else:
                # raises RuntimeError in case the dns_node string
                # does not refer to a valid Dim Dns node
                pydim.dis_set_dns_node(dns_node)
        except RuntimeError as e:
            print "No Dim DNS node found. Please set the environment variable DIM_DNS_NODE"
            raise

        # Here we define the Services
        #---------------------------------------------------------------
        self.services = {
            'service_desc'  : Service(name='SERVICE_DESC',
                                format='C',
                                callback=self.__service_desc_callback),
            'state_list'    : Service(name='STATE_LIST',
                                format='C',
                                callback=self.__state_list_callback),
            'message'       : Service(name='MESSAGE',
                                format='C',
                                callback=self.__message_callback),
            'state'         : Service(name='STATE',
                                format='C',
                                callback=self.__state_callback),
        }
        # And here we provide service descriptions as well as
        # descriptions for their parameters.
        self.services['service_desc'].description = "Descriptions of services or commands and there arguments"
        self.services['service_desc'].parameter_description['Description'] = {
            'type' : 'string',
            'text' : 'For a detailed explanation of the descriptive string see the class reference of DimDescriptionService.'
        }
        self.services['state_list'].description = 'Provides a list with descriptions for each service.'
        self.services['state_list'].parameter_description['StateList'] = {
            'type' : 'string',
            'text' : 'A \\n separated list of the form id:name=description'
        }
        self.services['message'].description = 'A general logging service providing a quality of service (severity)'
        self.services['message'].parameter_description['Message'] = {
            'type' : 'string',
            'text' : 'The message'
        }
        self.services['state'].description = 'Provides the state of the state machine as quality of service.'
        self.services['state'].parameter_description['Text'] = {
            'type' : 'string',
            'text' : 'A human readable string sent by the last state change.'
        }
        #---------------------------------------------------------------


        # Let's also define some states for our server:
        self.states = States()
        self.states.start_up = State('StartUp', -1, 'just starting up')
        self.states.ready = State('Ready', 0, 'ready to run')
        self.states.running = State('Running', 1, 'running, ready to stop')
        self.states.ERROR = State('ERROR', 255, 'common ERROR state')
        self.states.FATAL = State('FATAL', 65535, 'fata lerror occured.')
        # more states can be added later in a the specific implementation
        # easily

        self._state = self.states.start_up

    def add_service(self, svc):
        """ to be used in __init__ of the implementation of the specific class
        """
        if not isinstance(svc, Service):
            raise TypeError("svc must be of type 'State' but is "+str(type(svc)))
        self.services[svc.name.lower()] = svc
        self._auto_update_services.append(svc.name.lower())

    def update(self):
        for s_name in self._auto_update_services:
            pydim.dis_update_service( self.services[s_name].id )

    def start_serving(self):
        """
        This function must be called before any
        pydim.dis_update_service is being called in the specific base class
        """
        for key in self.services:
            svc = self.services[key]
            svc.id = pydim.dis_add_service(self.name+'/'+svc.name,
                                    svc.format,
                                    svc.callback,
                                    svc.tag)

            if not svc.id:
                raise Exception("An error occurred while registering "+svc.name)
            pydim.dis_update_service(svc.id)

        pydim.dis_start_serving(self.name)

    # private interface ------------------------------------------------
    def __service_desc_callback(self, tag):
        """
        This callback function provides the service description string as
        requested by the FACT++ dim service std.
        The string should never change during the lifetime of a server, since
        it is highly unlikely for a server to change its services.

        The format of a service description is:
        SVR_NAME/SVC_NAME=general description|PAR_NAME[type]:parameter description\n

        The 2nd part from PAR_NAME to "parameter descripter" may occur repeatedly,
        delimited by '|' characters.

        Each service description is delimited from the next by the '\n' character.
        The entire string should be '\0' terminated, as usual in the C-world.
        """

        description_string = ""
        for svc_name in self.services:
            svc = self.services[svc_name]
            description_string += self.name + '/' + svc.name + '=' + svc.description
            for p_name in svc.parameter_description:
                p_desc = svc.parameter_description[p_name]
                description_string += '|' + p_name + '[' + p_desc['type'] + ']:' + p_desc['text']
            description_string += '\n'
        description_string += '\0'

        return (description_string, )

    def __message_callback(self, tag):
        """
        The logging message system devised by the authors of FACT++
        is entirely based on their decision to implement every DimServer
        as a state machine.
        In such a design, state transitions and their reasons can be communicated
        using the MESSAGE service.
        However, the STATE service does the same, while already providing
        information about the transition reason as well.

        In our opinion the MESSAGE service can still be useful for general
        purpose logging of debug messages. However since it is
        so general purpose, it can only be of limited use for non developers,
        since the meaning of the MESSAGES may change drastically from server to
        server.

        The callback function thus has been implemented such that
        **if** the server instance has a 'message' attribute,
        its string represenation will be published.
        If not, the message is: "No Message avaliable.\0"

        We don't know if there is a fixed format for the message string,
        but an Example looks like this:
        "State Transition from Connected[2] to VoltageOn[5] (by Run())\0"
        """
        try:
            msg = str(self.message)
            if msg[-1] != '\0':
                msg += '\0'
        except AttributeError:
            msg = "No Message avaliable.\0"
        return (msg, )

    def __state_list_callback(self, tag):
        """
        This method provides a list of all possible states of this
        server. Since the definition of states are is ultimately up to
        the designer of the specific server.
        We have three possibilities to deal with it in this base class.

        A) We require the designer of the specific class to overwrite this method
            so it returns the state_list string in the correct format.

        B) We ask the designer of the specific class to provide another
            kind of representation of the states, maybe a dict like:
            self.states_list = {
                'Ready' : {'id' :   0, 'text' : 'common ready state'},
                'Error' : {'id' : 255, 'text' : 'common ERROR state'},
                ...
            }
            From this dict, we create the correctly formated state_list.

        C) We provide a common way of implementing FSMs, and by using an
            abstract base class FSM, the designer of the base class
            provides us with the necessary information to create the
            state list

        For the moment we just provide a state list, that says, that there
        are is only one State: The *Unknown* state.

        state list format:
        signed integer:state_name=state description\n

        state list ends with '\0'
        """
        state_list = ""
        for state_name in self.states.__dict__:
            state = getattr(self.states, state_name)
            state_list += str(state.id)+':'+state_name+'='+state.text+'\n'
        state_list += '\0'
        return (state_list, )

    def __state_callback(self, tag):
        """
        This mothod provides a string representation of the current server
        state. We don't know the format of the state string represenation
        but an Example looks like this:
        "DriveOff[12] by Run()\0"

        According to the SERVICE_DESC of this service:
        Provides the state of the state machine as quality of service.

        Apart from that, everything we wrote in the __doc__ string of
        state_list_callback() is true also here.

        """
        pydim.dis_set_quality( self.services['state'].id, self._state.id )
        return (self._state.name+'['+str(self._state.id)+']\0', )







import time
import numpy as np
class MyServer( FactppBase, threading.Thread ):

    def __init__(self, name):
        # We inherit from FactppBase and from Thread, so we need to call
        # both their __init__ functions. The order should not matter
        threading.Thread.__init__(self)
        FactppBase.__init__(self, name)
        self._stop = False

        # We need to define the services we want to publish here
        # For each service, let us define some instance attribute,
        # so we can think of a service, like a way to publish out internal
        # attributes
        self._example1 = ""         # <-- this will hold the current time
        self._example2_float = 0.   # <-- this will hold the arbitrary float, we don't assign None, since this is not a float
        self._example2_int = 0      # <-- this will hold the arbitrary int, we don't assign None, since this is not an int

        svc_ex1 = Service(name="EXAMPLE1",
                            format='C', callback=self.example1)
        svc_ex1.description="A simple service returning the time as a string. It updates now and then."
        svc_ex1.parameter_description = {
                                        'Time' : {'type':'string', 'text':'The time in std format :-)'}
                                        }

        svc_ex2 = Service(name="EXAMPLE2",
                            format='F:1;I:1', callback=self.example2)
        svc_ex2.description="A simple service returning a float and an int"
        svc_ex2.parameter_description = { 'aFloat' : {'type':'float', 'text' : 'some arbitrary float'},
                                            'anInt' : {'type': 'int', 'text' : 'some arbitrary int'}
                                        }
        # We should not forget to hook our two services into the
        # service list, which is provided and used by FactpBase.
        self.add_service( svc_ex1 )
        self.add_service( svc_ex2 )

    # Now we define the two callback functions for our services, but we do it a little
    # different than before
    def example1(self, tag):
        return (self._example1+'\0', )

    def example2(self, tag):
        return (self._example2_float, self._example2_int)

    def run(self):
        self.start_serving()
        while True:
            # We interact with our hardware and measure something, let's
            # say .. a float and an int...
            # also we 'measure' the time :-)

            self._example2_float = np.random.rand()
            self._example2_int = np.random.randint(100)
            self._example1 = time.asctime()
            self.update()
            time.sleep(5)

            if self._stop:
                print self.name, "is out of service... now."
                pydim.dis_stop_serving()
                break

    def stop(self):
        self._stop = True



if __name__ == "__main__":
    s = MyServer(name="peter")
    print "created MyServer with the name PETER"
    s.start()
    print "type: 's.stop()' to stop the DimServer"


